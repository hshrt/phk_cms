<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Asia/Hong_Kong" );  // http://www.php.net/manual/en/timezones.php
define( "DB_NAME", "BSPPHK");
define( "DB_DSN", "mysql:host=localhost;dbname=".DB_NAME.";charset=utf8" );
define( "DB_USERNAME", "kahoo" );
define( "DB_PASSWORD", "000000");

define( "DB_DSN_BPC", "mysql:host=192.168.5.17;dbname=BPC_local;charset=utf8" );
define( "DB_USERNAME_BPC", "marco" );
define( "DB_PASSWORD_BPC", "marco");

define( "DB_DSN_WEATHER", "mysql:host=192.168.5.17;dbname=Weather;charset=utf8" );
define( "DB_USERNAME_WEATHER", "marco" );
define( "DB_PASSWORD_WEATHER", "marco");

//Video Injection server URL
define("VIDEO_INJECTION_URL", '172.30.6.162:8000/');
define("VIDEO_INJECTION_USER", 'app');
define("VIDEO_INJECTION_PASSWORD", '1234');
//http://172.30.6.162:8000/Video_Injection/default/

define("BASE_FOLDER", "cmsphk");
define("HOTEL_LOCATION", "hongkong");
define("SERVER_TYPE", "production");
define("HEADER_TEXT", "HSH PHK CMS");
define("VERSION_NUM", "1.2.10");

define( "ROOT_PATH", "http://192.168.5.220/cmsphk/" );
define( "ALL_ITEM", "get_all_item" );
define( "ALL_DICT", "get_all_dict" );
define( "ALL_ON_OFF_ITEM", "get_all_item_list" );

define("OK",200);
define("Created",201);
define("No_Content",204);
define("Not_Modified",304);
define("Bad_Request",400);
define("Unauthorized",401);
define("Forbidden",403);
define("Not_Found",404);
define("Method_Not_Allowed",405);
define("Gone",410);
define("Unsupported_Media_Type",415);
define("Unprocessable_Entity",422);
define("Too_Many_Requests",429);

define("Invalid_input",501);
define("Session_timeout",502);

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
?>
