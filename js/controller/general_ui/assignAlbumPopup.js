App.AssignAlbumPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    title: "",

    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.mediaId){
            this.fileName = options.fileName;
            this.mediaId = options.mediaId;
            this.album = options.album;
            console.log("fileName = " + this.fileName);
            console.log("mediaId = " + this.mediaId);
            console.log("album = " + this.album);
        }
        this.render();
    },
    events: {

        'click #closeBtn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render assignAlbumPopup");
        $.ajax({
         url : "php/html/setup/assignAlbumPopup.php",
         method : "GET",
         dataType: "html",
         data : {}
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    self.getAlbumList();
                    $("#create_box_title").text("Photo");




                    $("#saveCreateBtn").on('click',function(){

                        $.ajax({
                            url : "api/album/assignAlbum.php",
                            method : "POST",
                            dataType: "json",
                            data : {
                                album: $("#assignAlbumSection #albumSelectionBox").val(),
                                mediaId: self.mediaId

                            }
                        }).success(function(json){
                            console.log(json);

                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }
                            
                            self.destroy();
                            App.mediaView.refresh();
                            //location.reload();


                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    });


                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    getAlbumList: function(){
        var self = this;
        var _url = "api/album/getAllAlbum.php";

        $.ajax({
            url : _url,
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){

            App.hideLoading();

            console.log(json);
            self.albumObjs = json.data;

            //add the online video item into the selector box
            for (var x = 0; x<self.albumObjs.length; x++) {
                var curAlbumObj = self.albumObjs[x];
                $("#assignAlbumSection #albumSelectionBox").append($('<option />').text(curAlbumObj.name).val(curAlbumObj.id));
            }

            $("#assignAlbumSection #albumSelectionBox").val(self.album);

            console.log("jsonObj = " + self.albumObjs);

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});