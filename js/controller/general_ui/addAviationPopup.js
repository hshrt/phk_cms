App.AddAviationPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    title: "",

    aviObj: null,
    parentId: 0,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.title){

            this.title = options.title;
            this.aviObj = options.fxObj;
        }
        this.render();
    },
    events: {

        'click #closeBtn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render addAviationPopup");
        $.ajax({
         url : "php/html/setup/addAviationPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);



                    //which mean it is editing, so have preassigned value
                    if(self.aviObj != null){

                        $("#create_box_title").text(upperFirstChar(self.title));

                        $('#name').val(self.aviObj.name);
                        $('#iata').val(self.aviObj.IATA);

                        $("#saveCreateBtn").val("Create");
                    }
                    else{
                        $("#create_box_title").text("Create new " + upperFirstChar(self.title));
                        $("#deleteBtn").hide();
                    }


                    $("#saveCreateBtn").on('click',function(){
                        if($("#name").val() == "" || $("#iata").val().trim() == ""){
                            alert("Name or IATA code cannot be empty");
                            return;
                        }
                        $.ajax({
                            url : "api/flightStat/updateAviation.php",
                            method : "POST",
                            dataType: "json",
                            data : {
                                type: self.title,
                                itemId: self.aviObj==null?"":self.aviObj.id,
                                name: $('#name').val(),
                                iata: $('#iata').val()

                            }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            console.log(json);

                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }
                            
                            self.destroy();

                            //location.reload();

                            App.aviationList.destroy();
                            App.aviationList.initialize();

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    });

                    $("#deleteBtn").on('click',function(){
                        App.yesNoPopup = new App.YesNoPopup(
                            {
                                yesFunc:function()
                                {
                                    $.ajax({
                                        url : "api/flightStat/deleteAviation.php",
                                        method : "POST",
                                        dataType: "json",
                                        data : {
                                            type: self.title,
                                            itemId: self.aviObj==null?"":self.aviObj.id

                                        }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                                    }).success(function(json){
                                        console.log(json);

                                        if(json.status == 502){
                                            alert(App.strings.sessionTimeOut);
                                            location.reload();
                                            return;
                                        }

                                        self.destroy();

                                        //location.reload();

                                        App.aviationList.destroy();
                                        App.aviationList.initialize();

                                    }).error(function(d){
                                        console.log('error');
                                        console.log(d);
                                    });
                                },
                                msg:"Are you sure to delete this item?"
                            }
                        );
                    });

                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});