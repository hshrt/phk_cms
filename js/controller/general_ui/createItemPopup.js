App.CreateItemPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    title: "Page",
    showTypeBox:true,
    parentId: 0,
    infrasysElement: undefined,
    infrasys_type: 'I',
    infrasys_id: undefined,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.title){
            console.log("this title =" + options.title);
            this.title = options.title;

        }
        if(options && options.showTypeBox){
            this.showTypeBox = options.showTypeBox;
        }
        if(options && options.parentId){
            this.parentId = options.parentId;
            //alert("parent id = " + this.parentId);
            if(this.parentId !=0 && this.parentId != 1 &&  this.parentId != 2){
                this.showTypeBox = false;
            }
            //alert("showTypeBox = " + this.showTypeBox);
        }

        this.render();
    },
    events: {

        'click #cancel_btn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render createItemPopup");
        $.ajax({
         url : "php/html/createItemPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    $('.searchSelect').select2();

                    $('.searchSelect').on('select2:select', function (e) {
                        // alert(e.params.data.id);
                        self.infrasys_id = e.params.data.id;
                    });

                    $("#create_box_title").text("Create " + self.title);

                    if(!self.showTypeBox){
                        $('#selectionBox').hide();
                        $("#typeLabel").hide();

                        $("#titleContainer").css({width: "98%"});
                    }

					if(self.title.localeCompare("Choice") === 0){
                        var itemTypeList = $('.itemTypeList');
                        itemTypeList.show();
                        $.ajax({
                            url: "api/infrasys/getInfrasysElement.php",
                            method: "GET",
                            dataType: "json"
                        }).success(function(result){
                            infrasysElement = result.data;
                            self.infrasysElement = infrasysElement;
                            $('.searchSelect').find('option').remove();
                            for (var i = 0; i < infrasysElement.length; i++) {
                                if (infrasysElement[i].type === 'I') {
                                    var optionString = '<option value="' + infrasysElement[i].code + '">' + 
                                    infrasysElement[i].name + " ," + infrasysElement[i].code + " :" + infrasysElement[i].price + '</option>';
                                    $('.searchSelect').append(optionString);
                                }
                            }
                        }).error(function(d){;
                        });
					}

					$("#input1").click(function () {
                        if (self.infrasysElement != undefined) {
                            $('.searchSelect').find('option').remove();
                            for (var i = 0; i < self.infrasysElement.length; i++) {
                                if (self.infrasysElement[i].type === 'I') {
                                    var optionString = '<option value="' + self.infrasysElement[i].code + '">' + 
                                    self.infrasysElement[i].name + " ," + self.infrasysElement[i].code + " :" + self.infrasysElement[i].price + '</option>';
                                    $('.searchSelect').append(optionString);
                                }
                            }
                        }

                        self.infrasys_type = 'I';
					});

                    $('#input2').click(function() {
                        if (self.infrasysElement != undefined) {
                            $('.searchSelect').find('option').remove();
                            for (var i = 0; i < self.infrasysElement.length; i++) {
                                if (self.infrasysElement[i].type === 'M') {
                                    var optionString = '<option value="' + self.infrasysElement[i].code + '">' + 
                                    self.infrasysElement[i].name + " ," + self.infrasysElement[i].code + " :" + self.infrasysElement[i].price + '</option>';
                                    $('.searchSelect').append(optionString);
                                }
                            }
                        }

                        self.infrasys_type = 'M';
                    });

                    $("#closeBtn").on('click',function(){
                        console.log("closeBtn clicked");
                        self.destroy();
                    });

                    $("#createBtn").on('click',function(){
                        var type = $('#selectionBox :selected').text();
                        if(self.title == "Page"){
                            $('#selectionBox :selected').text();
                        }
                        else if(self.title == "Sections"){
                            type = "section";
                        }
                        else if(self.title == "Articles"){
                            type = "article";
                        }
                        else if(self.title == "Items"){
                            type = "item";
                        }
                        else if(self.title == "Services"){
                            type = "service";
                        }
                        else if(self.title == "Sub Items"){
                            type = "sub_item";
                        }
                        else if(self.title == "Choice"){
                            type = "choice";
                        }
                        else if(self.title == "Options"){
                            type = "option";
                        }
                        else if(self.title == "Dependences"){
                            type = "dependence";
                        }
                        else if(self.title == "Option set"){
                            type = "optionset";
                        }

                        var skipCreateDescription = 0;

                        //App.haveItem is true, which mean the current root is a type of "Information (top sub menu)", so we don't need to have Description for the Article, this article is the left menu item which is food sub category
                        if((App.haveItem && type != 'item' && type!='sub_item' ) || type == 'service')
                        {
                            skipCreateDescription = 1;
                        }

                        var data = {title: $('#title').val(), type: type, parentId: self.parentId, skipCreateDescription: skipCreateDescription};

                        if (self.title == "Choice") {
                            data.infrasys_id = $('.searchSelect').val();
                            if (self.infrasys_id != undefined) {
                                data.infrasys_id = self.infrasys_id;
                            }
                            
                            data.infrasys_type = self.infrasys_type;
                        }

                        App.showLoading();
                        $.ajax({
                            url : "api/createNewItem.php",
                            method : "POST",
                            dataType: "json",
                            data : data
                        }).success(function(json){
                            App.hideLoading();
                            console.log("the json = " + json);

                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }

                            console.log("the json status = " + json.status);
                            console.log("the json status = " + json.msg);
                            console.log("the json status = " + json.data);

                            if(json.status == 501){
                                alert(json.msg);
                                //App.showTipBox("fail-mode","Create Item failed",json.msg);
                                return;
                            }
                            self.destroy();

                            //location.reload();

                            App.libList.destroy();
                            App.libList.initialize();

                        }).error(function(d){
                            console.log('error')
                            console.log(d)
                        });
                    });
                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});