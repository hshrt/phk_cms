App.AddGuestPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    title: "Page",
    showTypeBox:true,
    parentId: 0,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.title){

            this.title = options.title;
        }
        this.render();
    },
    events: {

        'click #closeBtn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render addGuestPopup");
        $.ajax({
         url : "php/html//guest/addGuestPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    $("#create_box_title").text("Create new " + self.title);

                    //set date picker
                    $( "#checkInDateInput" ).datepicker({ dateFormat: 'yy-mm-dd' });
                    $( "#checkOutDateInput" ).datepicker({dateFormat: 'yy-mm-dd' });

                    $("#createBtn").on('click',function(){

                        $.ajax({
                            url : "api/guest/addGuest.php",
                            method : "POST",
                            dataType: "json",
                            data : {
                                roomNum: $('#roomNumInput').val(),
                                title: $('#salutationInput').val(),
                                firstName: $('#firstNameInput').val(),
                                lastName: $('#lastNameInput').val(),
                                arriveDate: $('#checkInDateInput').val() + " " + $('#checkInTimeInput').val(),
                                departureDate: $('#checkOutDateInput').val() + " " + $('#checkOutTimeInput').val(),
                                lastUpdateBy: 'ICS'

                            }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            console.log(json);
                            self.destroy();

                            //location.reload();

                            App.guestList.destroy();
                            App.guestList.initialize();

                        }).error(function(d){
                            console.log('error')
                            console.log(d)
                        });
                    });

                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});