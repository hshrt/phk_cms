App.GuestList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObj: null,
    jsonObj_filtered:null,
    sortType:'room',
    sortDirection: 'ASC',
    title: "Guest",
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){


        var self = this;

        $.ajax({
            url : "php/html/itemList.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            console.log(html);

            $(self.el).append(html).promise()
                .done(function() {
                    //alert("done");
                    self.postUISetup();
                    $("#boxer").after($("#itemListContainer"));
                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });



    },

    postUISetup:function(){
        var self = this;
        $("#item_list_head_title").text(self.title);

        $('#addBtn').on('click',function(){
            //alert("delete item id = " + App.currentId);
            self.showPopup();
        });

        //hide the addBtn, we can write data in the TC3 account system now, so data cannot be synchorized even we enable it.
        $('#addBtn').hide();

        //setup add btn text
        $('#addBtn').text("+Add " + self.title);

        App.showLoading();

        $.ajax({
            url : "api/guest/getAllGuest.php",
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){
            //setTimeout(App.hideLoading, 1000);

            App.hideLoading();

            console.log(json);
            App.guestList.jsonObj = json.data;

            self.drawGuestList(App.guestList.jsonObj);

            console.log(App.idArray);

            $("#guestname_sort_icon").hide();
            $("#chkin_sort_icon").hide();
            $("#chkout_sort_icon").hide();

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });

        $('#search').on('keyup', function() {
            if (this.value.length > 0) {

                self.jsonObj_filtered = new Array();
                self.jsonObj_filtered = searchStringInArrByName($("#search").val(),self.jsonObj);

                if(self.jsonObj_filtered.length == 0){
                    $("#resultTable").empty();
                    $("#noResultMsg").show();
                }
                else{
                    console.log("yeah");

                    $("#noResultMsg").hide();

                    self.filtering = true;

                    self.drawGuestList(self.jsonObj_filtered);
                }
            }
            else{
                $("#paginationContainer").show();
                $("#noResultMsg").hide();
                self.filtering = false;
                self.drawGuestList(self.jsonObj);
            }
        });
    },
    sortBy: function(type,order){
        console.log("order = " + order);
        this.sortDirection = order;
        if(type == "room"){
            if(order == 'ASC') {
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj1.room - obj2.room;
                });
            }
            else{
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj2.room - obj1.room;
                });
            }
        }
        else if(type == "guestname"){
            if(order == 'ASC') {
                //this.jsonObj.sort(function (obj1, obj2) {
                    //return obj1.guestname - obj2.guestname;
                //});

                function SortByName(a, b){
                    var aName = a.guestname.toLowerCase();
                    var bName = b.guestname.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByName);

            }
            else{
                function SortByNameRev(a, b){
                    var aName = b.guestname.toLowerCase();
                    var bName = a.guestname.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObj.sort(SortByNameRev);
            }
        }
        else if(type == "indate"){
            if(order == 'ASC') {
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj1.indate - obj2.indate;
                });
            }
            else{
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj2.indate - obj1.indate;
                });
            }
        }
        else if(type == "outdate"){
            if(order == 'ASC') {
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj1.outdate - obj2.outdate;
                });
            }
            else{
                this.jsonObj.sort(function (obj1, obj2) {
                    return obj2.outdate - obj1.outdate;
                });
            }
        }

        this.drawGuestList(this.jsonObj);
    },
    changeDate: function(str1){
        //var hour   = parseInt(str1.substring(6,10));    2014-11-25 12:00:00
    // str1 format should be dd/mm/yyyy. Separator can be anything e.g. / or -. It wont effect
        var year   = parseInt(str1.substr(0,4));
        var month  = parseInt(str1.substr(5,2));
        var day   = parseInt(str1.substr(8,2));
        var hour   = parseInt(str1.substr(11,2));
        var min   = parseInt(str1.substr(14,2));
        var sec  = parseInt(str1.substr(17,2));

        console.log("year = " + year);
        console.log("month = " + month);
        console.log("day = " + day);

        var date1 = new Date(year, month, day,hour,min,sec);
        return date1.getFullYear()+"-"+(date1.getMonth()+1) +"-"+date1.getDate() + " "+date1.getHours()+1<10?"0":""+date1.getHours()+1+":"+ date1.getMinutes()<10?"0":""+ date1.getMinutes();
    },
    sortButtonClick : function(_sortType,sortIcon){
        //alert("Yeah");
        console.log("hide all");
        $("#guestname_sort_icon").hide();
        $("#room_sort_icon").hide();
        $("#chkin_sort_icon").hide();
        $("#chkout_sort_icon").hide();

        $("#"+sortIcon).show();

        if(this.sortType == _sortType){
            if(this.sortDirection == 'ASC'){
                this.sortBy(_sortType,'DESC');
                $("#"+sortIcon).removeClass("glyphicon-chevron-up");
                $("#"+sortIcon).addClass("glyphicon-chevron-down");
            }
            else {
                this.sortBy(_sortType, 'ASC');
                $("#"+sortIcon).removeClass("glyphicon-chevron-down");
                $("#"+sortIcon).addClass("glyphicon-chevron-up");
            }
        }
        else{
            this.sortType = _sortType;
            this.sortBy(_sortType, 'ASC');
            $("#"+sortIcon).removeClass("glyphicon-chevron-down");
            $("#"+sortIcon).addClass("glyphicon-chevron-up");
        }

        $("#guestname_sort_icon").hide();
        $("#room_sort_icon").hide();
        $("#chkin_sort_icon").hide();
        $("#chkout_sort_icon").hide();

        $("#"+sortIcon).show();

    },
    drawGuestList: function(objects){

        var self = this;

        $("#resultTable").empty();

        //Insert Table Header
        var tableHeaderString = "<tr><th class='tableHead'><span id='nameText'>Name </span><span class='glyphicon glyphicon-chevron-up' id ='guestname_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='folioText'>Folio# </span><span class='glyphicon glyphicon-chevron-up' id ='chkin_sort_icon'></span></th>"+
			"<th class='tableHead'><span id='roomText'>Room# </span><span class='glyphicon glyphicon-chevron-up' id ='room_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='chkinText'>Check-In </span><span class='glyphicon glyphicon-chevron-up' id ='chkin_sort_icon'></span></th>"+
            "<th class='tableHead'><span id='chkoutText'>Check-Out </span><span class='glyphicon glyphicon-chevron-up' id ='chkout_sort_icon'></span></th>"+
			"<th class='tableHead'><span id='irdText'>Enable IRD?  </span><span class='glyphicon' id ='chkin_sort_icon'></span><input type='checkbox' id='irdChkBox' checked='checked'>All</th>"+
            "<th class='tableHead'><span id='penchatText'>Enable PENCHAT?  </span><span class='glyphicon' id ='chkin_sort_icon'></span><input type='checkbox' id='penchatChkBox' checked='checked'>All</th>" + 
            "<th class='tableHead'><span id='chkOutButton'>Check-Out </span><span class='glyphicon glyphicon-chevron-up'></span></th></tr>";


        $("#resultTable").append(tableHeaderString);

        $('#penchatChkBox').change(function() {
            var status = 'disable';
            if (this.checked) {
                status = 'enable';
            }
            App.showLoading();
            $.ajax({
                url : "api/guest/changeAllPenchatStatus.php",
                method : "POST",
                dataType: "json",
                data : {penchatStatus:status}
            }).success(function(json){
                //setTimeout(App.hideLoading, 1000);

                App.hideLoading();
                location.reload();

                console.log(json);

            }).error(function(d){
                App.hideLoading();
                console.log('error');
                console.log(d);
            }); 
        });
		
		$('#irdChkBox').change(function() {
			var status = 'disable';
			if (this.checked) {
				status = 'enable';
			}
			App.showLoading();
			$.ajax({
				url : "api/guest/changeAllIrdStatus.php",
				method : "POST",
				dataType: "json",
				data : {irdStatus:status}
			}).success(function(json){
				//setTimeout(App.hideLoading, 1000);

				App.hideLoading();
				location.reload();

				console.log(json);
				/*App.guestList.jsonObj = json.data;

				self.drawGuestList(App.guestList.jsonObj);

				console.log(App.idArray);

				$("#guestname_sort_icon").hide();
				$("#chkin_sort_icon").hide();
				$("#chkout_sort_icon").hide();*/

			}).error(function(d){
				App.hideLoading();
				console.log('error');
				console.log(d);
			});			
		});

        $("#nameText").on('click',function(){

            self.sortButtonClick('guestname',"guestname_sort_icon");
        });

        $("#roomText").on('click',function(){

            self.sortButtonClick('room',"room_sort_icon");
        });
        $("#chkinText").on('click',function(){

            self.sortButtonClick('indate',"chkin_sort_icon");
        });
        $("#chkoutText").on('click',function(){

            self.sortButtonClick('outdate',"chkout_sort_icon");
        });
		
		var isAllTrue = true;
        var isAllPenchatTrue = true;
        for(var y = 0; y<objects.length;y++){
            //console.log("currentIndex = " + y);
            
            var guestObj = objects[y];

            //if chkin is null, it is not actually a room, dont need to add a record
            if(guestObj.chkin == null){
                continue;
            }
            $("#resultTable").append(
                "<tr id='"+guestObj.id+"' style='height:30px'><td>"+"<div class ='titleContainer'></div>"+"<div class='titleText'>"+guestObj.guestname + "</div></div>"+"</td>"+
                    "<td>"+guestObj.folio+"</td>"+
					"<td>"+guestObj.room+"</td>"+
                    "<td>"+guestObj.indate.substr(0,4) +"-"+ guestObj.indate.substr(4,2)+"-"+ guestObj.indate.substr(6,2)+"</td>" + "<td>"+guestObj.outdate.substr(0,4) +"-"+ guestObj.outdate.substr(4,2)+"-"+ guestObj.outdate.substr(6,2)+"</td>" +
					"<td>"+"<select id='ird_"+guestObj.room+"'>"+
							"<option value='enable'>Enable</option>"+
							"<option value='disable'>Disable</option>"+
						"</select>"+"</td>"+
                    "<td>"+"<select id='penchat_"+guestObj.room+"'>"+
                        "<option value='enable'>Enable</option>"+
                        "<option value='disable'>Disable</option>"+
                    "</select>"+"</td>"+
                    "<td>"+"<div id='chkOut_"+guestObj.room+"' class='btn bg-red'>"+"Check-Out"+"</td>"+
                    "</tr>");
					
			$("#ird_"+guestObj.room).val(guestObj.enable_ird);
            $('#penchat_'+guestObj.room).val(guestObj.enable_penchat);
            $("#"+guestObj.id).attr( "index", y );
			
			if (guestObj.enable_ird == 'disable') {
				isAllTrue = false;
			}

            if (guestObj.enable_penchat == 'disable') {
                isAllPenchatTrue = false;
            }

            $("#chkOut_"+guestObj.room).on('click',function(){
                var myId = $(this).attr('id');
                App.yesNoPopup = new App.YesNoPopup(
                    {
                        yesFunc:function()
                        {
                            var myRoom = myId.substr(myId.indexOf('_')+1);

                            $.ajax({
                                url : "api/guest/checkoutGuest.php",
                                method : "POST",
                                dataType: "json",
                                data : {roomNum:myRoom}
                            }).success(function(json){
                                console.log(json);
                                App.yesNoPopup.destroy();
                            }).error(function(d){
                                App.hideLoading();
                                console.log('error');
                                console.log(d);
                            });
                        },
                        msg:"Are you really sure to check-out this room? (Please note that changes here will be reflected after several minutes)"
                    }
                );
            });

            // todo
			$("#ird_"+guestObj.room).on('change',function(){

				var myId = $(this).attr('id');
				var val = this.value;
				var myRoom = myId.substr(myId.indexOf('_')+1);

				//App.showLoading();

				$.ajax({
					url : "api/guest/changeIrdStatus.php",
					method : "POST",
					dataType: "json",
					data : {room:myRoom,ird:val}
				}).success(function(json){
					//setTimeout(App.hideLoading, 1000);

					//App.hideLoading();

					console.log(json);
					$('#irdChkBox').prop('checked', false);
					/*App.guestList.jsonObj = json.data;

					self.drawGuestList(App.guestList.jsonObj);

					console.log(App.idArray);

					$("#guestname_sort_icon").hide();
					$("#chkin_sort_icon").hide();
					$("#chkout_sort_icon").hide();*/

				}).error(function(d){
					App.hideLoading();
					console.log('error');
					console.log(d);
				});
			});

            $("#penchat_"+guestObj.room).on('change',function(){

                var myId = $(this).attr('id');
                var val = this.value;
                var myRoom = myId.substr(myId.indexOf('_')+1);

                //App.showLoading();

                $.ajax({
                    url : "api/guest/changePenchatStatus.php",
                    method : "POST",
                    dataType: "json",
                    data : {room:myRoom,penchat:val}
                }).success(function(json){
                    //setTimeout(App.hideLoading, 1000);

                    //App.hideLoading();

                    console.log(json);
                    $('#penchatChkBox').prop('checked', false);
                    /*App.guestList.jsonObj = json.data;

                    self.drawGuestList(App.guestList.jsonObj);

                    console.log(App.idArray);

                    $("#guestname_sort_icon").hide();
                    $("#chkin_sort_icon").hide();
                    $("#chkout_sort_icon").hide();*/

                }).error(function(d){
                    App.hideLoading();
                    console.log('error');
                    console.log(d);
                });
            });

            //disable click function, we can't write data in the TC3 account system now, so data cannot be synchorized even we enable it.
            /*(function (jsonitem) {
                //alert(Area);
                $("#"+jsonitem.id).on('click',function(){

                    App.currentId = $(this).attr('id');
                    App.currentItem = jsonitem;
                    //alert(jsonObj.result[y].id);
                    window.location = document.URL+"/"+$(this).attr('id');
                });
            })(objects[y]);*/
        }
		$('#irdChkBox').prop('checked', isAllTrue);
        $('#penchatChkBox').prop('checked', isAllPenchatTrue);
        $("#resultTable").find('*').css({cursor:"default"});
        $("#nameText,#roomText,#chkinText,#chkoutText").css({cursor:"pointer"});

    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.addGuestPopup = new App.AddGuestPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        $("#item_list_boxer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});