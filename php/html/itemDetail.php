<div class="box detail_container" id=''>
    <div class="box-header" style="padding-top:0px">
        <div style='width:100%;height:auto;float:left'>
            <h2 id='itemName'></h2>
            <select id='lang_selectionBox' name='language'>

                <option value='en'>English</option>
                <option value='zh_cn'>Simplied Chinese</option>
                <option value='fr'>French</option>
                <option value='jp'>Japanese</option>
                <option value='ar'>Arabic</option>
                <option value='es'>Spanish</option>
                <option value='de'>German</option>
                <option value='ko'>Korean</option>
                <option value='ru'>Russian</option>
                <option value='pt'>Portuguese</option>
                <option value='zh_hk'>Traditional Chinese</option>
                <option value='tr'>Turkish</option>
                <option value='my'>Burmese</option>

            </select>
        </div>
    </div>
    <div class="box-body">
        <div class='half_width_container form-group' id = 'container_en'>
            <label for='title' id='titleLabel_en'>Title</label>
            <input id='detailTitle_en' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
            </div>
        <div class='half_width_container form-group' id = 'container_lang'>
            <label for='title' id='titleLabel_lang'>Title</label>
            <input id='detailTitle_lang' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus='' disabled>
            </div>

		<br>
        <div class='full_width_container full_width_container_popup' id= 'container_infrasysID'>
            <label for='title' >InfrasysID</label><br>
            <select id="infrasysID" name="type" style="width: 300px;">
            </select>
        </div>
		<br>

		<div id="itemTypeList"> 
			<label for="menuItem"> 
				<input name="store" id="input3" type="radio" value="I" checked="checked" /> 
				Menu Item</label> 
				<br>
			<label for="modifierItem"> 
				<input name="store" id="input4" type="radio" value="M"/> 
				Modifier Item</label> 
				<br>
            <select style="width: 300px;" name="search-alias-menu" id="com" class="js-example-basic-single searchSelect">
              <option value="AL">Alabama</option>
              <option value="AL1">Alabama1</option>
              <option value="AL2">Alabama2</option>
              <option value="AL3">Alabama3</option>
              <option value="AL4">Alabama4</option>
              <option value="WY">Wyoming</option>
            </select>
		</div>
		<br>

        <div class='half_width_container form-group' id = 'textEditor_en'>
            <label id = 'des_en'>
                Description
            </label>
            <div id="textareaContainer">
                <textarea cols="80" id="editor1" name="editor1" rows="10">
                </textarea>
            </div>
        </div>
        <div class='half_width_container form-group' id = 'textEditor_lang'>
            <label  id = 'des_lang'>
                Description
            </label>
            <div id="textareaContainer">
            <textarea cols="80" id="editor2" name="editor2" rows="10">

            </textarea>
             </div>
        </div>

        <div class='half_width_container form-group' id = 'priceSection'>
            <div>
                Price
            </div>
            <input id='price' class='form-control' type='text' name='price' tabindex='1'  min="0" max="10000" value='' maxlength="6"
                   autofocus=''>
        </div>

        <div class='half_width_container form-group' id = 'commandSection'>
            <div id="commandText">
                Command
            </div>
            <input id='command' class='form-control' type='text' name='command' tabindex='1' value='' maxlength="50"
                   autofocus=''>
        </div>

        <div class='half_width_container form-group' id = 'shortDesSection'>
            <div>
                Short Description
            </div>
            <input id='shortDes' class='form-control' type='text' name='command' tabindex='1' value='' maxlength="50"
                   autofocus=''>
        </div>
        <div class='half_width_container form-group' id = 'printSection'>
            <div>
               
            </div>
            <input id="printCheckBox" type="checkbox" />
            <label>print</label>
        </div>

        <div class='half_width_container form-group' id = 'canOrderSection'>
            <div>

            </div>
            <input id="canOrderCheckBox" type="checkbox" />
            <label>can be ordered by BSP</label>
        </div>

        <div class="half_width_container form-group" id="container_start_time">
            <label>Start time 1</label>
            <input type="text" id="start_time" data-format="HH:mm" data-template="HH : mm" name="datetime">
        </div>

        <div class="half_width_container form-group" id="container_end_time">
            <label>End time 1</label>
            <input type="text" id="end_time" data-format="HH:mm" data-template="HH : mm" name="datetime">
        </div>

        <div class="half_width_container form-group" id="container_start_time2">
            <label>Start time 2</label>
            <input type="text" id="start_time2" data-format="HH:mm" data-template="HH : mm" name="datetime">
        </div>

        <div class="half_width_container form-group" id="container_end_time2">
            <label>End time 2</label>
            <input type="text" id="end_time2" data-format="HH:mm" data-template="HH : mm" name="datetime">
        </div>

        <div class='half_width_container form-group' id = 'videoInjectionSection'>

            <label>Video</label></br>
            <select class="form-control" id='videoSelectionBox' name='video'>
                <option value=''>none</option>
            </select>
        </div>


        <div class='sub_section'>Media</div>
        <!--<div class='dash'></div>-->
        <div id='ImageContainer'><div id='addImageBtn'></div></div>

        <div class='sub_section' id="iconSection">Icon</div>
        <!--<div class='dash'></div>-->
        <div id='IconContainer'><div id='addIconBtn'></div></div>

        <div class='sub_section' id="videoThumbnailSection">Video Thumbnail</div>
        <!--<div class='dash'></div>-->
        <div id='VideoThumbnailContainer'><div id='addVdoThnBtn'></div></div>

        <div class="half_width_container form-group" id="container_min">
            <label>Minimum of choice(s)</label>
            <select class="form-control" id="min_selector">

            </select>
        </div>
        <div class="half_width_container form-group" id="container_max">
            <label>Maximum of choice(s)</label>
            <select class="form-control" id="max_selector">

            </select>
        </div>

        <div class="half_width_container form-group" id="container_max_quantity">
            <label>Maximum quantity</label>
            <select class="form-control" id="max_quantity_selector">

            </select>
        </div>

        <div class='half_width_container form-group' id = 'complexFoodSection'>
            <div>

            </div>
            <input id="complexOptionCheckBox" type="checkbox" />
            <label>complex option</label>
        </div>




        <div style="width:100%;float:left" id="optionsContainer">

            <div class='sub_section form-group' id="optionSetsText">Option Sets</div>
            <div class='half_width_container'>
                All option sets
            </div>
            <div class='half_width_container'>
                Selected option sets
            </div>
            <div class='half_width_container list form-group' id="option_list">
                <div class = 'fixList' id="option_fixList">
                </div>

            </div>

            <div class='half_width_container list form-group' id="selected_option_guest_list">
                <div class = 'fixList' id="selected_option_fixList">
                </div>

            </div>
        </div>




        <div class='save_cancel_container'>
            <div class='round_btn btn bg-maroon' id='delete_btn'>Delete Section</div>
            <div class='round_btn btn bg-olive' id='save_btn'>Save Section</div>
            <div style='float:right' class='submit round_btn btn bg-light-blue' id="syncBtn">Sync with Infrasys</div>
            <div class='round_btn btn bg-navy' id='preview_btn'>Tablet Preview</div>
            <div class='round_btn btn bg-navy' id='print_preview_btn'>Print Preview</div>
            </div>
        </div>
    </div>
</div>

