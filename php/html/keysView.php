
<div id="keysViewContainer">
    <div class="box" id="keys_boxer">
        <div class="box-header" style="padding-top:0px">
            <h3 style="float:left" id="keys_head_title">Locale Keys</h3>
            <div class='submit round_btn btn bg-olive' id='addBtn'>+Add Locale</div>

            <input id="search" class="form-control" type='text' name='title' tabindex='1' data-type='text' placeholder="Search..." autofocus=''>
        </div>

        <div class="box-body">
            <div id="headerContainer">
                <div id="keyTitle" class="tableHead">Key</div>
                <div id="engTitle" class="tableHead">English</div>
            </div>

            <div id="itemContainer">

            </div>
            <div id="noResultMsg">There are no locales</div>
            <div id='paginationContainer'>

            </div>
        </div>

        <div class="box-footer">

        </div><!-- /.box-footer-->
    </div>
</div>
