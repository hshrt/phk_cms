<div class='popup_box_container' id="yes_no_root">
        <div class='popup_box yes_no'>
            <h3 id="msg">Are you sure to delete?</h3>
            <div class='save_cancel_container'>
                <div class='round_btn btn bg-navy' id='cancel_btn'>Cancel</div>
                <div class='round_btn btn bg-red' id='confirm_btn'>Confirm</div>
                </div>
        </div>
</div>

