<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box popup_big'>
        <div class='closeBtn' id='closeBtn'></div>
        <div id="optionsArea">
            <input id="imageOption" type="checkbox" name="vehicle" value="Bike" checked>
            <div id="imageText">Image</div>
            <input id="descriptionOption" type="checkbox" name="vehicle" value="Car" checked>
            <div id="desText">Description</div>
            <input id="highlightOption" type="checkbox" name="vehicle" value="Car" checked>                <div id="highlightText">Selection</div>

        </div>
        <div id="tabletBG">
            <div id="appBGContainer">
                <div id="appBG"></div>
                <div id="itemImage"></div>
                <div id="overlayBox"></div>
                <img id="arrowBtn" src="images/info_close.png"/>
                <div id="zoom_container">
                    <img id="zoomOutBtn" src="images/font_sm.png"/>
                    <img id="zoomInBtn" src="images/font_lrg.png"/>
                </div>
                <div id="description_text_container">
                    <div id="description_text"></div>
                </div>
                <div id="itemTitle">
                    <div id="itemTitleText"></div>
                </div>

                <div id="parentTitle"></div>

                <div id="foodText"></div>
                <div id="priceText"></div>

                <div id="greyPopupBox">
                    <img id="closeBtn" src="images/close_btn.png"/>
                    <div id="title"></div>
                    <div id="price"></div>
                    <div id="description"></div>
                </div>

                <div id="overlayBox1"></div>
                <div id="overlayBox2"></div>
                <div id="overlayBox3"></div>
                <div id="overlayBox4"></div>
                <div id="overlayBox5"></div>
                <div id="overlayBox6"></div>


                <!--<div id="dimmer"></div>-->
            </div>

        </div>

    </div>
</div>