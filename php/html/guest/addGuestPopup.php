<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <h1 id="create_box_title"></h1>
        <div class='closeBtn' id='closeBtn'></div>

        <div class='half_width_container half_width_container_popup' id= 'container_salutation'>
            <label for='title' id='salutationLabel'>Salutation</label>
            <input id='salutationInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_firstName'>
            <label for='title' id='firstNameLabel'>First Name</label>
            <input id='firstNameInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_lastName'>
            <label for='title' id='lastNameLabel'>Last Name</label>
            <input id='lastNameInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_roomNum'>
            <label for='title' id='roomNumLabel'>Room Number</label>
            <input id='roomNumInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>

        <div class='half_width_container half_width_container_popup' id= 'container_checkIn'>
                <label for='title' id='checkInDateLabel'>Check In Date</label>
                <input id='checkInDateInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>

        </div>

        <div class='half_width_container half_width_container_popup' id= 'container_checkInTime'>
            <label for='title' id='checkInTimeLabel'>Check In Time</label>
            <input id='checkInTimeInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>

        <div class='half_width_container half_width_container_popup' id= 'container_checkOut'>
                <label for='title' id='checkOutDateLabel'>Check Out Date</label>
                <input id='checkOutDateInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_checkOutTime'>
            <label for='title' id='checkOutTimeLabel'>Check Out Time</label>
            <input id='checkOutTimeInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>

        <div class="actions">
            <div style='float:right' class='submit round_btn btn btn bg-olive' id="createBtn">Create</div>
        </div>
    </div>
</div>