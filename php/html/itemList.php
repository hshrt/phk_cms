<div class="box" id="item_list_boxer">
    <div class="box-header" style="padding-top:0px">

            <h3 style="float:left" id="item_list_head_title">Library</h3>

            <div style="margin-top:20px" class='submit btn bg-olive' id='addBtn'>+Add Page</div>
            <input id="search" class="form-control" type="text" name="title" tabindex="1" data-type="text" placeholder="Search..." autofocus="">
    </div>
    <div class="box-body">

        <div class = "row" id="tableContainer">
            <table class="table table-striped" id="commonTable">
                </table>
            <table class="table table-striped" id='resultTable'>
                </table>
            <div id="noResultMsg">No Result</div>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer">

    </div><!-- /.box-footer-->
</div>
