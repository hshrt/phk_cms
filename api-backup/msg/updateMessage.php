<?php 

ini_set( "display_errors", true );

require( "../../config.php" );
require( "../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$msgId = isset($_POST['msgId'])?$_POST['msgId']:null;
$guestMsgIdList = isset($_POST['guestMsgIdList'])?$_POST['guestMsgIdList']:null;

$startDate = isset($_POST['startDate'])?$_POST['startDate']:null;
$endDate = isset($_POST['endDate'])?$_POST['endDate']:null;
$priority = isset($_POST['priority'])?$_POST['priority']:null;
$boardcast = isset($_POST['boardcast'])?$_POST['boardcast']:null;
$popup = isset($_POST['popup'])?$_POST['popup']:null;
$isScheduled = isset($_POST['isScheduled'])?$_POST['isScheduled']:0;


//setup DB
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "DELETE FROM roomMessageMap where messageId = :id";

$st = $conn->prepare ($sql);

$st->bindValue( ":id", $msgId, PDO::PARAM_STR );

//only delete the guestMessageMap when it is not a boardcast message
if($boardcast != 1) {
    $st->execute();
}

$guestMsgIdList = explode(",", $guestMsgIdList);


if($boardcast == 0){
    $sql = "INSERT INTO roomMessageMap (room,messageId,lastUpdate, lastUpdateBy) VALUES ";


    //echo("GuestMsgIDList = ".$guestMsgIdList);

    for($x = 0;$x<sizeof($guestMsgIdList);$x++){
        $sql = $sql."('".$guestMsgIdList[$x]."','".$msgId."',"."now()".",'".$_SESSION['email']."')";
        if($x != sizeof($guestMsgIdList)-1){
            $sql = $sql.",";
        }
    }

    //echo $sql;

    $st = $conn->prepare ($sql);

    if($msgId != null) {
        $st->execute();
    }
}

$sql = "UPDATE message SET startDate=:startDate,endDate=:endDate,priority=:priority,boardcast=:boardcast, isPopup=:popup, lastUpdate=now(),lastUpdateBy=:lastUpdateBy, isScheduled=:isScheduled where id = :msgId;";

//echo $sql;

$st = $conn->prepare ($sql);

$st->bindValue( ":msgId", $msgId, PDO::PARAM_STR );
$st->bindValue( ":startDate", $startDate, PDO::PARAM_STR );
$st->bindValue( ":endDate", $endDate, PDO::PARAM_STR );
$st->bindValue( ":priority", $priority, PDO::PARAM_STR );
$st->bindValue( ":boardcast", $boardcast, PDO::PARAM_STR );
$st->bindValue( ":isScheduled", $isScheduled, PDO::PARAM_INT );
$st->bindValue( ":popup", $popup, PDO::PARAM_INT );
$st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );

$st->execute();


if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'update message good');
}
else{
    echo returnStatus(1 , 'update message fail');
}


$conn = null;


?>
