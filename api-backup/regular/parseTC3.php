<?php
/**
 * Created by PhpStorm.
 * User: esd
 * Date: 9/22/14
 * Time: 10:12 AM
 */
ini_set( "display_errors", true );

chdir(__DIR__);
require( "../../config.php" );
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//for production
/*require( "/var/www/html/cmsphk/config.php");
require("/var/www/html/cmsphk/php/inc.appvars.php");
require("/var/www/html/cmsphk/php/func_nx.php");*/

function file_list($d,$x){
    $l = array();
    foreach(array_diff(scandir($d),array('.','..')) as $f)if(is_file($d.'/'.$f)&&(($x)?ereg($x.'$',$f):1))$l[]=$d.$f;
    return $l;
}

//get last TC3 update Time
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT lastTC3time from monitor";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();
pprint_r($list);

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$lastTC3time = $list[0]["lastTC3time"];

$conn = null;

//copy all the rows from BPC database
$conn = new PDO( DB_DSN_BPC, DB_USERNAME_BPC, DB_PASSWORD_BPC );
$conn->exec("set names utf8");

$sql = "select * from allroom";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

pprint_r($list);
//pprint_r($list);

$conn = null;

//insert into CMS database
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "TRUNCATE allroom;";
$st = $conn->prepare ( $sql );
$st->execute();

for($x=0;$x<sizeof($list);$x++){
    $item = $list[$x];

    $sql = "INSERT INTO allroom (room,type,chkin,indate,outdate,faxnum,guestname,lang,country,groupid) VALUES
    (:room,:type,:chkin,:indate,:outdate,:faxnum,:guestname,:lang,:country,:groupid)";
    $st = $conn->prepare ( $sql );


    if(DB_NAME == "BSPPHK"){
        $st->bindValue( ":room", $item['roomNum'], PDO::PARAM_STR);
        $st->bindValue( ":type", $item['division'], PDO::PARAM_STR);
        $st->bindValue( ":chkin", $item['chkin'], PDO::PARAM_STR);
        $st->bindValue( ":indate", $item['indate'], PDO::PARAM_STR);
        $st->bindValue( ":outdate", $item['outdate'], PDO::PARAM_STR);
        $st->bindValue( ":faxnum", $item['faxNum'], PDO::PARAM_STR);
        $st->bindValue( ":guestname", $item['guestname'], PDO::PARAM_STR);
        $st->bindValue( ":lang", $item['lang'], PDO::PARAM_STR);
        $st->bindValue( ":country", $item['country'], PDO::PARAM_STR);
        $st->bindValue( ":groupid", $item['groupid'], PDO::PARAM_STR);
    }
    else{
        $st->bindValue( ":room", $item['room'], PDO::PARAM_STR);
        $st->bindValue( ":type", $item['type'], PDO::PARAM_STR);
        $st->bindValue( ":chkin", $item['chkin'], PDO::PARAM_STR);
        $st->bindValue( ":indate", $item['indate'], PDO::PARAM_STR);
        $st->bindValue( ":outdate", $item['outdate'], PDO::PARAM_STR);
        $st->bindValue( ":faxnum", $item['faxnum'], PDO::PARAM_STR);
        $st->bindValue( ":guestname", $item['guestname'], PDO::PARAM_STR);
        $st->bindValue( ":lang", $item['lang'], PDO::PARAM_STR);
        $st->bindValue( ":country", $item['country'], PDO::PARAM_STR);
        $st->bindValue( ":groupid", $item['groupid'], PDO::PARAM_STR);
    }
    /*for PBH, PCH*/
    /**/

    /*special for PHK*/


    $st->execute();

    if($st->rowCount()  > 0)
        echo 'update ok!';
    else
        echo 'not ok';
}

$conn = null;


//for PCH, PBJ production env
$dir_array = array("../../../../../../mnt/tpc/fid/fax/");

//for PHK production env
if(DB_NAME == "BSPPHK") {
    $dir_array = array(
        "../../../../../../../mnt/tpc/fid/tc3msg/");
}

//for PCH both testing  env
/*$dir_array = array("../../../../../tmp/test/f15/",
    "../../../../../tmp/test/f16/",
    "../../../../../tmp/test/f17/");*/

//temporary test on R&T local
/*$dir_array = array(
    "../tmp/fid/f1/",
    "../tmp/fid/f2/",
    "../tmp/fid/f3/",
    "../tmp/fid/f4/",
    "../tmp/fid/f5/");*/

//for PHK production env
/*$dir_array = array(
                    "../../../../../../mnt/tpc/fid/f5/");*/


//this will be a variable store in DB to represent last checking time
$last_check_time = strtotime($lastTC3time);


$tc3files = array();

foreach($dir_array as $dir){
    //pprint_r(file_list($dir,".rec"));
    $tc3files = array_merge($tc3files,file_list($dir,".rec"));
}

pprint_r($tc3files);

$toExecuteFileArray = array();

foreach($tc3files as $filename){

    echo $filename;

    //extractFileName($filename);

    if (file_exists($filename)) {
        clearstatcache();
        //echo "$filename was last modified: " . date ("Y-m-d H:i:s", filemtime($filename)).PHP_EOL;
        //echo $last_check_time.'<br>';
        //filter out all file which is added beyond the lastCheck time

        //the position to start get filename
        $startPointFromBack = 0;
        $fileLengthToExtract = 0;
        for($x=strlen($filename)-1;$x>=0;$x--){
            $startPointFromBack++;
            if (substr($filename,$x,1) == '/'){

                break;
            }
        }
        //echo PHP_EOL."Start Point from back = ".$startPointFromBack.PHP_EOL;

        if($startPointFromBack == 11){
            $fileLengthToExtract = 6;

        }
        else if($startPointFromBack == 12){
            $fileLengthToExtract = 7;
        }

        if(strtotime(date ("Y-m-d H:i:s", filemtime($filename))) > $last_check_time){
            echo('push!<br>');
            //array_push($toExecuteFileArray,strtotime(date ("Y-m-d H:i:s", filemtime($filename))) => $filename);
            $toExecuteFileArray[strtotime(date ("Y-m-d H:i:s", filemtime($filename))).substr($filename,
                    sizeof($filename)-11,6)] = $filename;
        }
        else{
            echo PHP_EOL."File time is before last check time".PHP_EOL;
        }

        ksort($toExecuteFileArray);
    }
}

pprint_r($toExecuteFileArray);
echo PHP_EOL."size of executeFileArray = " . sizeof($toExecuteFileArray);


$num = 0;

$dir = "";

$actionType =0;

foreach($toExecuteFileArray as $key => $filename){


    echo sizeof($filename) - sizeof($filename) -1;
    echo PHP_EOL."************file name = " .substr($filename,sizeof($filename)-11,6)."</br>".PHP_EOL;
    echo PHP_EOL."************file name command = " .substr($filename,sizeof($filename)-11,3)."</br>".PHP_EOL;

    echo PHP_EOL."substr = ".substr(extractFileName($filename),0,3).PHP_EOL;

    if(substr($filename,sizeof($filename)-11,3) == "100"){

        continue;

        echo("we do check in<br>");
        //$num++;

        $str = file_get_contents($filename);

        $roomNum = trim(substr($str,3,8));

        $regNum = trim(substr($str,11,8));

        $name = trim(substr($str,19,40));

        $nameArray = explode(',',$name);

        $title = $nameArray[0];
        $lastName = trim($nameArray[1]);

        $arriveDate = trim(substr($str,59,8));

        $departureDate = trim(substr($str,67,8));

        $checkOutAllow = substr($str,209,1);

        $firstCheckIn = substr($str,210,1);

        $lang = trim(substr($str,228,3));
        echo $str.'<br>';
        echo strlen($str).'<br>';
        echo '$departureDate = '.$departureDate .'<br>';
        echo '$firstCheckIn = '.$firstCheckIn .'<br>';
        echo '$arriveDate = '.$arriveDate .'<br>';
        echo '$lang = '.$lang .'<br>';
        echo '$checkOutAllow = '.$checkOutAllow .'<br>';
        echo '$title = '.$title .'<br>';
        echo '$lastName = '.$lastName .'<br>';
        echo 'guest name = '.$name .'<br>';
        echo 'reg number = '.$regNum .'<br>';
        echo 'room number = '.$roomNum .'<br>';

        insertIntoGuest($roomNum,$regNum,$lang,$title,$lastName,$arriveDate,$departureDate);
        //insertIntoGuest();


        //echo('$fp = '.$fp);
    }
    //this is for updating the guest record
    else if(substr($filename,sizeof($filename)-12,3) == "101"){

        continue;

        $str = file_get_contents($dir.$filename);
        echo('<br>try the second line <br>');

        if($str >= 272){

            $startIndex = strlen($str)-272;

            $roomNum = trim(substr($str,$startIndex+3,8));
            echo 'room number = '.$roomNum .'<br>';

            $regNum = trim(substr($str,$startIndex+11,8));
            echo 'reg number = '.$regNum .'<br>';

            $name = trim(substr($str,$startIndex+19,40));
            echo 'guest name = '.$name .'<br>';

            $nameArray = explode(',',$name);

            $title = $nameArray[0];
            $lastName = trim($nameArray[1]);

            echo '$title = '.$title .'<br>';
            echo '$lastName = '.$lastName .'<br>';

            $arriveDate = trim(substr($str,$startIndex+59,8));
            echo '$arriveDate = '.$arriveDate .'<br>';

            $departureDate = trim(substr($str,$startIndex+67,8));
            echo '$departureDate = '.$departureDate .'<br>';

            $checkOutAllow = substr($str,$startIndex+209,1);
            echo '$checkOutAllow = '.$checkOutAllow .'<br>';

            $firstCheckIn = substr($str,$startIndex+210,1);
            echo '$firstCheckIn = '.$firstCheckIn .'<br>';

            echo('$filename'.$filename);
            //if(substr($filename,0,6) == "101401"){
                echo('updateGuest pre fire!!!');
                updateGuest($roomNum,$regNum,$title,$lastName,$arriveDate,$departureDate);
            //}
        }
    }
    //this is checkout of guest record
    else if(substr($filename,sizeof($filename)-12,3) == "102"){

        continue;

        echo 'have 102  ';

        $str = file_get_contents($dir.$filename);
        echo strlen($str);

        if($str >= 21){
            $roomNum = trim(substr($str,3,8));
            echo 'room number = '.$roomNum .'<br>';

            $regNum = trim(substr($str,11,8));
            echo 'reg number = '.$regNum .'<br>';

            $isRoomVacant = trim(substr($str,19,1));
            echo 'guest name = '.$name .'<br>';

            checkoutGuest($roomNum,$regNum);
        }
    }
    //this is move room record
    else if(substr($filename,sizeof($filename)-12,3) == "104"){

        continue;

        echo 'have 104  ';
        $str = file_get_contents($dir.$filename);

        if($str >= 30){
            $roomNum = trim(substr($str,3,8));
            echo 'new room number = '.$roomNum .'<br>';

            $regNum = trim(substr($str,11,8));
            echo 'reg number = '.$regNum .'<br>';

            $oldRoomNum = trim(substr($str,19,8));
            echo 'old Room Num = '.$oldRoomNum .'<br>';

            $isOldRoomVacant = trim(substr($str,27,1));
            echo '$isOldRoomVacant = '.$isOldRoomVacant .'<br>';

            $isNewRoomVacant = trim(substr($str,28,1));
            echo '$isNewRoomVacant = '.$isNewRoomVacant .'<br>';
        }

        moveRoom($roomNum,$regNum,$oldRoomNum,$isOldRoomVacant,$isNewRoomVacant);
    }
    //this is message record
    else if(substr(extractFileName($filename),0,3) == "200"){
        $str = file_get_contents($dir.$filename);

        $firstSixChar = extractFileName($filename);
        echo("firstSixChar ".$firstSixChar);
        echo("add message fire");

        //$pos = strpos($str, $firstSixChar);
        //echo("fuck = <br>" );
        //echo("pos = " .$pos. "<br>");
        //pprint_r($pos);

        $posArray = getocurence($str,$firstSixChar);
        pprint_r($posArray);

        for($x = 0;$x<sizeof($posArray);$x++){
        //break;
            $pos = $posArray[$x];

            if($str >= 30){
                $roomNum = trim(substr($str,$pos+3,8));
                echo 'new room number = '.$roomNum .'<br>';

                $regNum = trim(substr($str,$pos+11,8));
                echo 'reg number = '.$regNum .'<br>';

                $messageId = trim(substr($str,$pos+19,8));
                echo 'message ID = '.$messageId .'<br>';

                $dateReceive = trim(substr($str,$pos+27,8));
                echo '$DateReceive = '.$dateReceive .'<br>';

                $timeReceive = trim(substr($str,$pos+35,5));
                echo '$timeReceive = '.$timeReceive .'<br>';

                $msgLength = trim(substr($str,$pos+81,3));
                echo '$msgLength = '.$msgLength .'<br>';

                $msg = '';
                //if message is valid then input to database
                if($msgLength >= 2 && $msgLength<=1200){
                    $msg = trim(substr($str,$pos+84,$msgLength));



                    //to avoid the case that the length of message given by TC3 is wrong

                    $posArray2 = getocurence($msg,$firstSixChar);

                    if(sizeof($posArray2) > 0){
                        $msg = trim(substr($msg,0,$posArray2[0]));
                    }


                    echo '$msg = '.$msg .'<br>';
                }

                addMessage($roomNum,$regNum,$messageId,$dateReceive,$timeReceive,$msg);
            }
        }
    }
    //update message
    else if(substr($filename,sizeof($filename)-12,3) == "203"){

        continue;

        $str = file_get_contents($dir.$filename);

        $firstSixChar = substr($str,0,6);
        echo("firstSixChar ".$firstSixChar);

        //$pos = strpos($str, $firstSixChar);
        //echo("fuck = <br>" );
        //echo("pos = " .$pos. "<br>");
        //pprint_r($pos);

        $posArray = getocurence($str,$firstSixChar);
        pprint_r($posArray);

        for($x = 0;$x<sizeof($posArray);$x++){
            //break;
            $pos = $posArray[$x];

            if($str >= 40){
                $roomNum = trim(substr($str,$pos+3,8));
                echo 'new room number = '.$roomNum .'<br>';

                $regNum = trim(substr($str,$pos+11,8));
                echo 'reg number = '.$regNum .'<br>';

                $messageId = trim(substr($str,$pos+19,8));
                echo 'message ID = '.$messageId .'<br>';

                $status = trim(substr($str,$pos+27,1));
                echo '$status = '.$status .'<br>';

                $dateReceive = trim(substr($str,$pos+28,8));
                echo '$DateReceive = '.$dateReceive .'<br>';

                $timeReceive = trim(substr($str,$pos+36,6));
                echo '$timeReceive = '.$timeReceive .'<br>';

                updateMessage($roomNum,$regNum,$messageId,$status,$dateReceive,$timeReceive);
            }
        }
    }

    $actionType = substr($filename,sizeof($filename)-12,3);

    //after process the file, delete the file
    echo PHP_EOL."unlink result= " .unlink($filename);

}

if(sizeof($toExecuteFileArray) > 0){
    //**update lastHandleTC3 Type
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");


    $sql = "UPDATE monitor SET lastExecuteTC3time=now(),lastHandleTC3Type=".$actionType;
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $conn = null;
}

//delete message where room is checked out
//get all the rows from allroom
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "select * from allroom";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$room_to_delete_list = array();

for($x=0;$x<sizeof($list);$x++){
    $item = $list[$x];
    //echo PHP_EOL.$item["room"]." = ".strlen($item["guestname"]).PHP_EOL;

    if(strlen($item["guestname"])==0){
        array_push($room_to_delete_list,$item["room"]);
    }
}
echo PHP_EOL."START DELETE".PHP_EOL;
//array_push($room_to_delete_list,322);

pprint_r($room_to_delete_list);

//set the expired msg to isScheduled=0
$sql = "update message set isScheduled=0 where endDate <= now() and isScheduled=1;";
$st = $conn->prepare ( $sql );
$st->execute();

for($y=0;$y<sizeof($room_to_delete_list);$y++){
    $sql = "select * from message m left join roomMessageMap rmm on m.id = rmm.messageId where rmm
    .room=$room_to_delete_list[$y] AND m.isScheduled=0;";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $msglist = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $msglist[] = $row;
    }
    for($z=0;$z<sizeof($msglist);$z++){
        echo 'checkout room'. PHP_EOL;
        $sql = "UPDATE message SET status='D', lastUpdate=now() where id = :id AND boardcast != 2 AND isScheduled=0";
        $st2 = $conn->prepare ( $sql );
        $st2->bindValue( ":id",  $msglist[$z]['id'], PDO::PARAM_STR );
        $st2->execute();

        if (sizeof($msglist)>0){
            $sql = "DELETE FROM roomMessageMap where room = $room_to_delete_list[$y] AND messageId = :id";
            $st3 = $conn->prepare ( $sql );
            $st3->bindValue( ":id",  $msglist[$z]['id'], PDO::PARAM_STR );
            $st3->execute();
        }
    }
        $sql = "UPDATE orders SET deleted=1 where room = $room_to_delete_list[$y]";
        $st4 = $conn->prepare ( $sql );
        $st4->execute();
}

$conn = null;

//try to update the last updatetime Only when there is execution.
if(sizeof($toExecuteFileArray) > 0) {
//**update last TC3 update Time
    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");


    $sql = "UPDATE monitor SET lastTC3time=now()";
    $st = $conn->prepare($sql);
    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    $conn = null;
}
//**update last TC3 update Time

function insertIntoGuest($roomNum,$regNum,$lang,$title,$lastName,$arriveDate,$departureDate){
    echo 'insertIntoGuest';
    $data = array ('roomNum' => $roomNum, 'regNum' => $regNum, 'lang' => $lang, 'title'=>$title,
        'lastName'=> $lastName,
        'arriveDate'=>$arriveDate, 'departureDate'=>$departureDate, 'lastUpdateBy'=>'TC3');
    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(ROOT_PATH.'api/guest/addGuest.php', 'r', false, $context);

    $response = stream_get_contents($fp);
    pprint_r($response);
}

function updateGuest($roomNum,$regNum,$title,$lastName,$arriveDate,$departureDate){

    echo('updateGuest fire!!!');
    //setup input parameter
    $data = array ('roomNum' => $roomNum, 'regNum' => $regNum, 'title'=>$title,
        'lastName'=> $lastName,
        'arriveDate'=>$arriveDate, 'departureDate'=>$departureDate, 'update'=>1, 'lastUpdateBy'=>'TC3');
    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(ROOT_PATH.'api/guest/addGuest.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);
}
function checkoutGuest($roomNum,$regNum){
    //setup input parameter

    echo ('checkoutGuest fire!');
    $data = array ('roomNum' => $roomNum, 'regNum' => $regNum, 'lastUpdateBy'=>'TC3');
    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(ROOT_PATH.'api/guest/checkoutGuest.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);
}

function moveRoom($roomNum,$regNum,$oldRoomNum,$isOldRoomVacant,$isNewRoomVacant){
    echo ('moveRoom fire!');
    $data = array ('roomNum' => $roomNum, 'regNum' => $regNum, 'oldRoomNum'=>$oldRoomNum, 'isOldRoomVacant'=>$isOldRoomVacant,
        'isNewRoomVacant'=>$isNewRoomVacant, 'lastUpdateBy'=>'TC3');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(ROOT_PATH.'api/moveRoom.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);
}

function addMessage($roomNum,$regNum,$messageId,$startDate,$endDate,$msg){
    //setup input parameter

    echo ('addMessage fire first!');
    $data = array ('roomNum' => $roomNum, 'regNum' => $regNum, 'messageId'=>$messageId, 'startDate'=>$startDate,
        'endDate'=>$endDate, 'msg'=>$msg, 'source'=>'HOTEL', 'lastUpdateBy'=>'TC3', 'type'=>3);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(ROOT_PATH.'api/msg/addMessage.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);
}

function updateMessage($roomNum,$regNum,$messageId,$status,$dateReceive,$timeReceive){
    //setup input parameter

    echo ('updateMessage fire!');
    $data = array ('roomNum' => $roomNum, 'regNum' => $regNum, 'messageId'=>$messageId,
        'status'=>$status, 'dateReceive'=>$dateReceive,
        'timeReceive'=>$timeReceive, 'lastUpdateBy'=>'TC3');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(ROOT_PATH.'api/msg/updateMessage.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);
}

function extractFileName($fullfileName){
    $startPoint = 0;
    $endPoint = 0;

    $stop = 0;
    for($x=strlen($fullfileName)-1;$x>=0;$x--){
        $startPoint++;

        if($stop == 0)
            $endPoint++;

        if (substr($fullfileName,$x,1) == '/'){

            break;
        }
        if(substr($fullfileName,$x,1) == '.'){
            $stop = 1;
        }
    }
    echo PHP_EOL."startPoint = " .$startPoint.PHP_EOL;
    echo PHP_EOL."endPoint = " .$endPoint.PHP_EOL;
    echo PHP_EOL."extracted = " .substr($fullfileName,strlen($fullfileName)-$startPoint+1,$startPoint-$endPoint-1).PHP_EOL;

    return substr($fullfileName,strlen($fullfileName)-$startPoint+1,$startPoint-$endPoint-1);
}

//echo('$num = '.$num);
//echo('$num = '.$num);


?>