<?php

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");

require_once "../php/UATCtrl.php";
require_once "../php/func_nx.php";
require_once "../php/func_json.php";

require("../php/lib/resize.class.php");

session_start();


        /*resizeSavePhoto('../upload/'.$file_name_big , 300 ,'../upload/'.$file_name_small);*/

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select fileName from media";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

foreach($list as $file){

    $file_name_prefix =   '_' .time() . '_' . rand(10000,99999);
    $file_name_big = $file["fileName"]. '.jpg';
    $file_name_medium = $file["fileName"] . '_m.jpg';

    pprint_r($file_name_big);
    pprint_r($file_name_medium);
    resizeSavePhoto('../upload/'.$file_name_big , 1024 ,'../upload/'.$file_name_medium);

    echo "resize .$file_name_medium. Done";
}

pprint_r($list);
exit;



function resizeSavePhoto($filename, $new_w, $out_filename){

    $file = $filename;

    list($width , $height) = getimagesize($file);
    $ratio = $new_w / $width;
    //$new_w = $width * $ratio ;
    $new_h = $height * $ratio;


    $params = array(
        'constraint' => array('width' => $new_w, 'height' => $new_h)
    );
    img_resize($file, $out_filename, $params);
}


//echo  '{"'.'result'.'":'.json_encode($list).'}';


?>
