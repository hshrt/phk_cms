<?php

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();
include("checkSession.php");

$mediaId = $_POST['mediaId'];
$itemId= $_POST['itemId'];
$type= $_POST['type'];
$isIcon = 0;
$isThumb = 0;

if ( empty($mediaId)){
    echo returnStatus(0, 'missing_media id');
    exit;
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $sql = "SELECT COUNT(*) from mediaItemMap where itemId = :itemId";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":itemId", $itemId.'', PDO::PARAM_STR );
    $st->execute();

    $prefer = 0;
    if($st->fetchColumn()== 0){
        $prefer = 1;
    }

    if($type == "icon"){
        $prefer = 0;
        $isIcon = 1;
        $isThumb = 0;
    }

    if($type == "thumb"){
        $prefer = 0;
        $isIcon = 0;
        $isThumb = 1;
    }

    $sql = "INSERT INTO mediaItemMap (mediaId,itemId,lastUpdateTime,lastUpdateBy,prefer, isIcon,isThumbnail ) VALUES (:mediaId, :itemId, now(),
:email, :prefer,:isIcon,:isThumbnail)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":mediaId", $mediaId.'', PDO::PARAM_STR );
    $st->bindValue( ":itemId", $itemId.'', PDO::PARAM_STR );
    $st->bindValue( ":prefer", $prefer, PDO::PARAM_INT );
    $st->bindValue( ":isIcon", $isIcon, PDO::PARAM_INT );
    $st->bindValue( ":isThumbnail", $isThumb, PDO::PARAM_INT );
    $st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );

    $st->execute();

    if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1 , 'add photo for item succeed');
    }
    else{
        echo returnStatus(0 , 'add photo for item fail');
    }
}
?>
