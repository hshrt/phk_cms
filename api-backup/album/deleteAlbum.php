<?php

ini_set( "display_errors", true );
require("../../config.php");
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$itemId = isset($_POST['itemId'])?$_POST['itemId']:null;


if (empty($itemId)){
    echo returnStatus(0, 'missing itemId');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "update media SET album = '' where album = :id";

$st = $conn->prepare ( $sql );

$st->bindValue( ":id", $itemId, PDO::PARAM_STR );

$st->execute();

$sql = "DELETE FROM album where id = :id";

$st = $conn->prepare ( $sql );

$st->bindValue( ":id", $itemId, PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount() > 0)
    echo returnStatus(1 , 'delete album ok!');
else
    echo returnStatus(0 , 'delete album fail! ');

$conn = null;

?>
