<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
//include("checkSession.php");

$id= isset($_REQUEST['orderId'])?$_REQUEST['orderId']:'';

//filter by status
$status = isset($_REQUEST['status'])?$_REQUEST['status']:0;


if (empty($id)){
    echo returnStatus(0, 'missing order Id');
}


else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");


    $sql = "UPDATE orders SET status=:status,lastUpdateBy=:email,lastUpdate=now()";


    $sqlEnd = "where id = :orderId;";


    $sql = $sql.$sqlEnd;

    $st = $conn->prepare ( $sql );
    $st->bindValue( ":orderId",$id, PDO::PARAM_STR );
    $st->bindValue( ":status",$status, PDO::PARAM_INT );
    $st->bindValue( ":email",$_SESSION['email'], PDO::PARAM_STR );
    //$st->bindValue( ":email","BSP", PDO::PARAM_STR );

    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'update Order OK',$list);
    }
    else{
        echo returnStatus(0, 'update Order fail',$list);
    }
}
return 0;

?>
