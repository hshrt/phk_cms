<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$room = isset($_REQUEST['room'])?$_REQUEST['room']:'';
$orderTime= isset($_REQUEST['orderTime'])?$_REQUEST['orderTime']:'';
$lastUpdateBy = isset($_REQUEST['lastUpdateBy'])?$_REQUEST['lastUpdateBy']:'BSP';

/*************************************
** new check on the order time diff - by william at 2018-04-23
*************************************/

function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    $h_diff = $interval->format("%h") * 60;
    $m_diff = $interval->format("%i");

    return intval($h_diff) + intval($m_diff);

}

$ordPhasedTime=0;
$ordPhasedTime=date("Y-m-d H:i", strtotime($orderTime));



if ( empty($room)){
    echo returnStatus(0, 'missing room number');
}
else if( intval(dateDifference($ordPhasedTime, date("Y-m-d H:i"), "%h %i")) > 10 ){
    echo returnStatus(0, 'The order time is wrong.');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    //*****create Order
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];

    $sql = "insert orders (id,room, foodIdList, deliveryTime, orderTime, numOfGuest,quantity, status, lastUpdate, 
lastUpdateBy) 
VALUES 
(:id,
:room,'','',:orderTime,0,0,3,now(),:lastUpdateBy)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":room",$room, PDO::PARAM_STR );
    $st->bindValue( ":orderTime",$orderTime, PDO::PARAM_STR );
    $st->bindValue( ":lastUpdateBy",$lastUpdateBy, PDO::PARAM_INT );
    $st->execute();

    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'Order submission OK');

        // hard coded the path 
		$url = "http://127.0.0.1/cmsphk/api/ird/getOrder.php?status=0&room=".$room."&date=".date("Y-m-d");

        $pullorder = file_get_contents($url);
        //var_dump(json_decode($justOrder, true));
        
        $json=json_decode($pullorder, true);
        
		$text = "";
        $file =  "/tmp/".$room."irdmail.txt";

        $myfile = fopen($file, "w") or die("Unable to open file!");
        fwrite($myfile, $text);
        fclose($myfile);

        exec(" cat /tmp/".$room."irdmail.txt | mail -s 'IRD mail - ".$room."' ''williamchan@peninsula.com,jameschen@peninsula.com,jasonwong@peninsula.com' ");

    }
    else{
        echo returnStatus(0, 'Order submission fail');
    }

    // do not care about order status, just send 188 to hotsos
    $hotsos = "http://172.30.6.13/hotsos.php?id=tempuser&pw=temppw&room=" .
                    $room . "&value=189";

    file_get_contents($hotsos);
}
return 0;

?>
