<?php
/**
 * Created by Sublime.
 * User: james
 * Date: 2/24/2016
 * Time: 11:40 AM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$room = isset($_REQUEST['room'])?$_REQUEST['room']:'';
$foodIdList = isset($_REQUEST['foodIdList'])?$_REQUEST['foodIdList']:'';
$orderTime= isset($_REQUEST['orderTime'])?$_REQUEST['orderTime']:'';
$deliveryTime= isset($_REQUEST['deliveryTime'])?$_REQUEST['deliveryTime']:'';
$numOfGuest = isset($_REQUEST['numOfGuest'])?$_REQUEST['numOfGuest']:1;
$quantity = isset($_REQUEST['quantity'])?$_REQUEST['quantity']:'';
$status = isset($_REQUEST['status'])?$_REQUEST['status']:0;
$lastUpdateBy = isset($_REQUEST['lastUpdateBy'])?$_REQUEST['lastUpdateBy']:'BSP';

// put orderId here, maybe just updating the status
$orderId = isset($_REQUEST['orderId'])?$_REQUEST['orderId']:'';

// get in-room dining requirement
$requirement = isset($_REQUEST['requirement'])?$_REQUEST['requirement']:'';

/*************************************
** new check on the order time diff - by william at 2018-04-23
*************************************/

function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    $h_diff = $interval->format("%h") * 60;
    $m_diff = $interval->format("%i");

    return intval($h_diff) + intval($m_diff);

}

$ordPhasedTime=0;
$ordPhasedTime=date("Y-m-d H:i", strtotime($orderTime));



if ( empty($room) and $orderId == ""){
    echo returnStatus(0, 'missing room number');
}
else if( intval(dateDifference($ordPhasedTime, date("Y-m-d H:i"), "%h %i")) > 10 and $orderId == ""){
    echo returnStatus(0, 'The order time is wrong.');
}
else if(strlen($foodIdList) == 0 and $orderId == ""){
    echo returnStatus(0, 'The food Id List is empty.');
}
else if(strlen($deliveryTime) == 0 and $orderId == ""){
    echo returnStatus(0, 'The delivery time is empty');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    //*****create Order
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }
    if ($orderId!="") {
        // need to get room, numOfGuest, quantity, requirement here
        $sql = "SELECT orders.room AS room, orders.foodIdList AS foodIdList, orders.numOfGuest AS numOfGuest, orders.quantity as quantity, order_requirement.text AS requirement FROM orders INNER JOIN order_requirement ON orders.id = order_requirement.id WHERE orders.id='" . $orderId . "'";
        $st = $conn->prepare ($sql);
        $st->execute();

        while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
            $room = $row["room"];
            $foodIdList = $row["foodIdList"];
            $numOfGuest = $row["numOfGuest"];
            $quantity = $row["quantity"];
            $requirement = $row["requirement"];
        }

        if (empty($room)) {
            $sql = "SELECT room, foodIdList, numOfGuest, quantity FROM orders WHERE orders.id='" . $orderId . "'";
            $st = $conn->prepare ($sql);
            $st->execute();

            while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
                $room = $row["room"];
                $foodIdList = $row["foodIdList"];
                $numOfGuest = $row["numOfGuest"];
                $quantity = $row["quantity"]; 
            }
        }
    } else {
        // need to insert order into db
        $uuid = $list[0]["UUID"];

        $sql = "insert orders (id,room, foodIdList, deliveryTime, orderTime, numOfGuest,quantity, status, lastUpdate, 
        lastUpdateBy) 
        VALUES 
        (:id,
        :room,:foodIdList,:deliveryTime,:orderTime,:numOfGuest,:quantity,:status,now(),:lastUpdateBy)";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
        $st->bindValue( ":room",$room, PDO::PARAM_STR );
        $st->bindValue( ":foodIdList",$foodIdList, PDO::PARAM_STR );
        $st->bindValue( ":deliveryTime",$deliveryTime, PDO::PARAM_STR );
        $st->bindValue( ":orderTime",$orderTime, PDO::PARAM_STR );
        $st->bindValue( ":numOfGuest",$numOfGuest, PDO::PARAM_INT );
        $st->bindValue( ":quantity",$quantity, PDO::PARAM_INT );
        $st->bindValue( ":status",$status, PDO::PARAM_INT );
        $st->bindValue( ":lastUpdateBy",$lastUpdateBy, PDO::PARAM_INT );
        $st->execute();

        if($st->fetchColumn() > 0 || $st->rowCount() > 0){
            echo returnStatus(1, 'Order submission OK');
        } else {
            echo returnStatus(0, 'Order submission fail');
        }
    }
    
    $jsonParam = new stdClass();
    $jsonParam->room = $room;
    $jsonParam->guest = $numOfGuest;
    $jsonParam->quantity = $quantity;
    $jsonParam->requirement = $requirement;
    $jsonParam->modifiers = [];

    // need to deal with foodIdList to get the infrasys_id
    $foodArray = explode(',', $foodIdList);

    for ($index = 0; $index < sizeof($foodArray); $index++) {
        $food = $foodArray[$index];
        $foodId = $food;

        // handle the ":" part
        $parseArray = explode(':', $food);

        if (sizeof($parseArray) > 1) {
            $foodId = $parseArray[0];
        }

        // handle the "/" part
        $parseSlashArray = explode('/', $food);

        if (sizeof($parseSlashArray) > 1) {
            $foodId = $parseSlashArray[sizeof($parseSlashArray) - 1];
        }

        $sql = "SELECT infrasys_type, infrasys_id FROM itemInfrasysMap WHERE item_id = '" . $foodId . "'";

        $st = $conn -> prepare($sql);
        $st -> execute();

        while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
            $current_infrasys_type = $row["infrasys_type"];
            $current_infrasys_id = $row["infrasys_id"];
            
            if ($current_infrasys_type === "I") {
                $jsonParam->code = $current_infrasys_id;
            } else if ($current_infrasys_type === "M") {
                array_push($jsonParam->modifiers, $current_infrasys_id);
            }
        }
    }

    // place order here
    $place_order_url = "http://localhost:4000/place_order";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $place_order_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonParam));

    // just match the timeout of 10 secs of bsp
    curl_setopt($ch, CURLOPT_TIMEOUT, 8);

    if ($orderId == "") {
        $result = curl_exec($ch);
        $result_json = json_decode($result);
        $result_status = "failed";
        if ( isset( $result_json->{'status'} ) ) {
            $result_status = $result_json->{'status'};
        }

        if ($result_status == "success") {
            // completed
            $status = 4;
        } else {
            // failed
            $status = 3;
        }
        curl_close($ch);

        $sql = "update orders set status=".$status." where id='".$uuid."'";
        $st = $conn->prepare( $sql );
        $st->execute();

        // hard coded the path 
        $url = "http://127.0.0.1/cmsphk/api/ird/getOrder.php?status=0&room=".$room."&date=".date("Y-m-d");

        $pullorder = file_get_contents($url);
        //var_dump(json_decode($justOrder, true));
        
        $json=json_decode($pullorder, true);
        
        $text = "";
        $file =  "/tmp/".$room."irdmail.txt";

        $myfile = fopen($file, "w") or die("Unable to open file!");
        fwrite($myfile, $text);
        fclose($myfile);

        foreach($json['data'] as $key=>$item) {

                if ( array_key_exists("choices",$item)) {

                        $text = $item['item']." - ".$item['choices']." [ deliveryTime:".$item['deliveryTime']." ]\r\n";
                } else {
                        $text = $item['item']." [ deliveryTime:".$item['deliveryTime']." ]\r\n";
                }

                echo $text;

                $myfile = fopen($file, "a") or die("Unable to open file!");
                fwrite($myfile, $text);
                fclose($myfile);

        }


        exec(" cat /tmp/".$room."irdmail.txt | mail -s 'IRD mail - ".$room."' 'williamchan@peninsula.com,jameschen@peninsula.com,jasonwong@peninsula.com' ");

        // in-room dining requirement
        if (!empty($requirement)) {
            $sql = "insert order_requirement (id, text)
            VALUES
            (:id, :text)";
            $st = $conn->prepare( $sql );
            $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
            $st->bindValue( ":text", $requirement, PDO::PARAM_STR );
            $st->execute();
        }
    } else {
        $result = curl_exec($ch);
        $result_json = json_decode($result);
        $result_status = "failed";

        if ( isset( $result_json->{'status'} ) ) {
            $result_status = $result_json->{'status'};
            echo $result;
        } else {
            echo "Please make sure infrasys gateway are up and running";
        }

        if ($result_status == "success") {
            // completed
            $status = 4;
        } else {
            // failed
            $status = 3;
        }
        curl_close($ch);
        $sql = "update orders set status=".$status." where id='".$orderId."'";
        $st = $conn->prepare( $sql );
        $st->execute();
    }
}
return 0;

?>
