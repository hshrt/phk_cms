<?php
require( "../config.php" );

ini_set( "display_errors", true );

require("../php/inc.appvars.php");
require("../php/func_nx.php");

$parentId = null;

if(isset($_REQUEST["parentId"])){
    $parentId = $_REQUEST["parentId"];
    $parentId=$parentId.'';
}

$appVersion = isset($_REQUEST["appVersion"])?$_REQUEST["appVersion"]:null;
if($appVersion!=null){
    $appVerNumArr=explode('.', $appVersion);
}


$lang = isset($_REQUEST["lang"]) ?$_REQUEST["lang"]:"en";

$exclusionKey = '8ce9537f-3493-11e7-a84b-0050568f5c44';

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT
		items.id, items.parentId, items.titleId, items.type, items.order, items.descriptionId, items.enable, items.layout, items.price, items.command, items.print,
		items.startTime, items.endTime, items.startTime2, items.endTime2, items.optionSetIds, items.maxChoice, items.minChoice, items.maxQuantity, items.canOrder, items.complexOption,
		items.videoId, items.availTime, items.lastUpdate, items.lastUpdateBy,

		title.id as title_id, title.en as title_en, title." . $lang . " as title_" . $lang . ",
		description.id as description_id, description.en as description_en, description." . $lang . " as description_" . $lang . ",

		fileName, iconName, videoThumbnail

	FROM items
	left join dictionary as title on items.titleId like title.id
	left join dictionary as description on items.descriptionId like description.id

	LEFT JOIN (select fileName AS iconName, fileExt as ext, m2.itemId from media inner join mediaItemMap m2
	on media.id = m2.mediaId && m2.isIcon = 1) d4
	ON items.id = d4.itemId

	LEFT JOIN (select fileName AS videoThumbnail, fileExt as ext, m3.itemId from media inner join mediaItemMap m3
	on media.id = m3.mediaId && m3.isThumbnail = 1) d5
	ON items.id = d5.itemId

	LEFT JOIN (select fileName, m1.itemId from media inner join mediaItemMap m1
	on media.id = m1.mediaId && m1.prefer = 1) d25
	ON items.id = d25.itemId

	where parentId LIKE :parentId
	order by items.order";

$st = $conn->prepare($sql);



if($parentId != null){
    $st->bindValue( ":parentId", $parentId, PDO::PARAM_INT );
}


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    if($row['id']!=$exclusionKey) {
        $list[] = $row;
    }
}

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get itemList good', $list);
}
else{
    echo returnStatus(0, 'get itemList fail');
}

$conn = null;


?>
