<?php 

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");

$mem_var = new Memcache;
$mem_var->connect("localhost", 11211);
// $cache = new MultipartCache();
// $cache->addserver("127.0.0.1");

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

    $sql = "SELECT  id,dictionary.key,en as E , zh_hk as CT, zh_cn as CS, jp as J, fr as F, ar as A, es as S, de as G, ko as K, 
ru as R, pt as P , tr as I from dictionary";

$st = $conn->prepare ($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
	$mem_var->set(ALL_DICT, $list);
	// $cache->set(ALL_DICT, $list);
    echo "done";
}

$conn = null;

?>