<?php

ini_set( "display_errors", true );
require("../config.php");
require("../php/inc.appvars.php");

session_start();

$appVersion = isset($_REQUEST["appVersion"])?$_REQUEST["appVersion"]:null;
if($appVersion!=null){
    $appVerNumArr=explode('.', $appVersion);
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select (select Max(lastUpdate)from dictionary) as dictLastUpdate , (select Max(lastUpdate)from items) as itemLastUpdate, (select Max(lastUpdateTime) from mediaItemMap)  as mediaLastUpdate;";
$st = $conn->prepare ( $sql );


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'Get allLastUpdateTime ok!',$list);
else
    echo returnStatus(0 , 'Get allLastUpdateTime fail!');

$conn = null;

?>
