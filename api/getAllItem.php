<?php
	require( "../config.php" );

	ini_set( "display_errors", true );

	require("../php/inc.appvars.php");
	require("../php/func_nx.php");

	$mem_var = new Memcache;
	$mem_var->connect("localhost", 11211);
	// $mem_var->set("test", "test123");

	$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
	$conn->exec("set names utf8");

	$exclusionKey = '8ce9537f-3493-11e7-a84b-0050568f5c44';

    $sql = "select items.*,t2.image,d4.iconName,d4.ext,d5.videoThumbnail, element.price AS infrasys_price, element.type AS infrasys_type from items

    LEFT JOIN

    (select id,itemId, filename as image, prefer from media, mediaItemMap where id = mediaId and media.delete!= 1 and mediaItemMap.prefer = 1 order by lastUpdateTime DESC) t2

    on items.id = t2.itemId

    LEFT JOIN (select fileName AS iconName,fileExt as ext, m2.itemId from media inner join mediaItemMap m2
            on media.id = m2.mediaId && m2.isIcon = 1) d4
            ON items.id = d4.itemId

    LEFT JOIN (select fileName AS videoThumbnail,fileExt as ext, m3.itemId from media inner join mediaItemMap m3
            on media.id = m3.mediaId && m3.isThumbnail = 1) d5
            ON items.id = d5.itemId
            
    LEFT JOIN itemInfrasysMap map1
    ON items.id = map1.item_id

    LEFT JOIN infrasys_items element
    ON map1.infrasys_id = element.code AND map1.infrasys_type = element.type

    where items.enable = 1
    
    GROUP BY items.id
    
    order by items.order ASC, items.lastUpdate ASC";

	$st = $conn->prepare($sql);
	$st->execute();

	$list = array();

	while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {


	    if($row['id']!=$exclusionKey || ($source!=null && $source== "cms")|| ($appVersion!=null && $appVerNumArr[0]>=4 &&
	            $appVerNumArr[1]>=8)
	    ) {
	        $list[] = $row;
	    }
	}

	if($st->fetchColumn() > 0 || $st->rowCount() > 0) {

	    $mem_var->set(ALL_ITEM, $list);
	    echo "done";
	}

	$conn = null;

?>
