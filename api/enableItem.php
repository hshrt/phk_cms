<?php

ini_set( "display_errors", true );
require( "../config.php" );
require("../php/inc.appvars.php");

session_start();
include("checkSession.php");

$itemId = isset($_POST['itemId'])?$_POST['itemId']:null;

if ( empty($itemId)){
    echo returnStatus(0, 'missing itemId ');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select enable from items where id = :itemId";
$st = $conn->prepare ( $sql );
$st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$enable = $list[0]["enable"];

$toAssignEnable = null;

if($enable == 1){
    $toAssignEnable = 0;
}
else{
    $toAssignEnable = 1;
}

if($itemId!=null){
    $sql = "UPDATE items SET enable=:enable,lastUpdate=now(),lastUpdateBy=:lastUpdateBy where id = :itemId";
}

$st = $conn->prepare ( $sql );
$st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );

$st->bindValue( ":enable", $toAssignEnable, PDO::PARAM_INT );
$st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

if($st->rowCount()  > 0)
    echo returnStatus(1 , 'update ok!',$toAssignEnable);
else
    echo returnStatus(0 , 'update fail! May be there is no change?');

$conn = null;

$feed = "http://" . $_SERVER['HTTP_HOST'] . "/cmsphk/api/getAllItem.php";
$result = file_get_contents($feed, true);

?>
