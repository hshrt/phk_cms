<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$room = isset($_REQUEST["room"])? $_REQUEST["room"]:null;

if($room == null){
    echo returnStatus(0 , 'No Room input!');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select * from stbToken where room = :room";
$st = $conn->prepare ( $sql );
$st->bindValue( ":room", $room, PDO::PARAM_INT);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'Get token ok!',$list);
else
    echo returnStatus(0 , 'Get token fail!');

$conn = null;

?>
