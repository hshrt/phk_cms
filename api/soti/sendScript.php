<?php

session_start();

require("../../php/inc.appvars.php");
require( "../../config.php" );

function sendScript($access_key, $deviceId, $fileName) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://192.168.5.126/mobicontrol/api/devices/" . $deviceId . "/actions",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\r\n  \"Action\": \"SendScript\",\r\n  \"Message\": \"installpackage /storage/emulated/0/bspPackage/" .$fileName." now\"\r\n}",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer " . $access_key,
		"cache-control: no-cache",
		"content-type: application/json"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}
}
function getDeviceId($sourceIP) {
	$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
	$conn->exec("set names utf8");
	$sql = "select deviceId from bsp_update_log where sourceIP=:sourceIP";
	$st = $conn->prepare ( $sql );
	$st->bindValue( ":sourceIP", $sourceIP, PDO::PARAM_STR );
	$st->execute();
	
	$list = array();

	while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
		$list[] = $row;
	}

	$deviceID = $list[0]["deviceId"];	
	return $deviceID;
}
	
$sourceIP = $_GET['sourceIP'];
$fileName = $_GET['fileName'];
$authurl = "https://192.168.5.126/mobicontrol/api/token";

$client_id = "756aace563514b9cadc2b168fb397eed";
$client_secret = "6YVgLHYq31m57RAgCZypPsYd9cnpnKrZ";

$data = array(
    'grant_type' => 'password',
    'username'   => 'wchan',
    'password'   => 'wchan',
	'client_id'   => '756aace563514b9cadc2b168fb397eed',
	'client_secret'   => '6YVgLHYq31m57RAgCZypPsYd9cnpnKrZ'
);

$ch = curl_init();
//Disable CURLOPT_SSL_VERIFYHOST and CURLOPT_SSL_VERIFYPEER by
//setting them to false.
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_URL, $authurl);
curl_setopt($ch, CURLOPT_POST, 1 );
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$auth = curl_exec( $ch );

if ( curl_errno( $ch ) ){
    echo 'Error: ' . curl_error( $ch );
}
curl_close($ch);

$secret = json_decode($auth);
$access_key = $secret->access_token;
$deviceId = getDeviceId($sourceIP);
sendScript($access_key, $deviceId, $fileName);

?>