<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$sourceIP = $_GET['sourceIP'];
$appVersion = $_GET['appVersion'];

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");
$sql = "update bsp_update_log set appVersion=:appVersion where sourceIP=:sourceIP";
$st = $conn->prepare ( $sql );

$st->bindValue( ":appVersion", $appVersion, PDO::PARAM_STR );
$st->bindValue( ":sourceIP", $sourceIP, PDO::PARAM_STR );
$st->execute();

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'update version success');
}
else{
    echo returnStatus(0, 'update version fail');
}

?>