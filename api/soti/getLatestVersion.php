<?php

session_start();

require("../../php/inc.appvars.php");

$dir = "/var/www/html/bspPackage";
$pattern = '\.(pcg)$';

$newstamp = 0;
$newname = "";
$dc = opendir($dir);
while ($fn = readdir($dc)) {
  # Eliminate current directory, parent directory
  if (ereg('^\.{1,2}$',$fn)) continue;
  # Eliminate other pages not in pattern
  if (! ereg($pattern,$fn)) continue;
  $timedat = filemtime("$dir/$fn");
  if ($timedat > $newstamp) {
    $newstamp = $timedat;
    $newname = $fn;
  }
}
echo $newname;
?>