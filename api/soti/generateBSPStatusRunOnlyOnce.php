<?php

ini_set( "display_errors", true );
require( "../../config.php" );


function fireAPI($access_key) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://192.168.20.154/mobicontrol/api/devices",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Authorization: Bearer " . $access_key,
		"cache-control: no-cache",
		"content-type: application/json"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  $decodedJson = json_decode($response, true);
	  $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
	  $conn->exec("set names utf8");
	  for ($x = 0; $x < count($decodedJson); $x++) {
		  $sql = "insert into bsp_update_log(sourceIP, deviceId, isOnline, macAddress, deviceName) VALUES(:sourceIP, :deviceId, :isOnline, :macAddress, :deviceName)";
		  $st = $conn->prepare ( $sql );
		  $st->bindValue( ":deviceId", $decodedJson[$x]["DeviceId"], PDO::PARAM_STR );
		  $st->bindValue( ":macAddress", $decodedJson[$x]["MACAddress"], PDO::PARAM_STR );
		  $st->bindValue( ":deviceName", $decodedJson[$x]["DeviceName"], PDO::PARAM_STR );
		  $st->bindValue( ":isOnline", $decodedJson[$x]["IsAgentOnline"], PDO::PARAM_STR );
		  $st->bindValue( ":sourceIP", $decodedJson[$x]["HostName"], PDO::PARAM_STR );
		  $st->execute();		  
	  }
	}
}

$authurl = "https://192.168.20.154/mobicontrol/api/token";

$client_id = "756aace563514b9cadc2b168fb397eed";
$client_secret = "6YVgLHYq31m57RAgCZypPsYd9cnpnKrZ";

$data = array(
    'grant_type' => 'password',
    'username'   => 'wchan',
    'password'   => 'wchan',
	'client_id'   => '756aace563514b9cadc2b168fb397eed',
	'client_secret'   => '6YVgLHYq31m57RAgCZypPsYd9cnpnKrZ'
);

$ch = curl_init();
//Disable CURLOPT_SSL_VERIFYHOST and CURLOPT_SSL_VERIFYPEER by
//setting them to false.
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_URL, $authurl);
curl_setopt($ch, CURLOPT_POST, 1 );
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$auth = curl_exec( $ch );

if ( curl_errno( $ch ) ){
    echo 'Error: ' . curl_error( $ch );
}
curl_close($ch);

$secret = json_decode($auth);
$access_key = $secret->access_token;

fireAPI($access_key);

?>