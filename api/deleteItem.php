<?php 

require("../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();
include("checkSession.php");

$id = $_POST["id"];

$type = $_POST["type"];

if($type == null){
    $type = "items";
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$delete_id_array = array();

//push the id of the delete item into the array
array_push($delete_id_array,$id);


if($type == "items"){
    $sql = "select items.id, items.parentID from items order by items.lastUpdate DESC";
    $st = $conn->prepare ( $sql );
    $st->execute();
    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
        //echo json_encode($row);
    }

    //recursive function to find all the ids of items need to be deleted
    function checkChild($list,$itemId,&$delete_id_array){
        foreach($list as $item){
            //echo('before '.$item['parentID'].'<br>');
            if($item['parentID'] == $itemId){
                //echo('inside'.$item['parentID'].'<br/>');

                array_push($delete_id_array,$item['id']);
                //pprint_r($delete_id_array);
                $_parentId = $item['id'];
                checkChild($list,$_parentId,$delete_id_array);
            }
        }
    }

    checkChild($list,$id,$delete_id_array);

    //echo('boboboy');
    //pprint_r($delete_id_array);

    //pprint_r($list);




foreach($delete_id_array as &$idItem){
    $idItem = "\"".$idItem."\"";
}

}

$delete_ids_string = implode(",",$delete_id_array);

//echo($delete_ids_string);

$sql = "DELETE items,
       dictionary
FROM items,
     dictionary
WHERE (items.titleId = dictionary.id  OR
      (items.descriptionId = dictionary.id AND items.descriptionId != 'descriptionIdTemp')) AND items.id IN ($delete_ids_string);

DELETE mediaItemMap
from mediaItemMap

where itemId IN ($delete_ids_string);";

//echo($sql);


if($type == "media"){
    $sql = "UPDATE media set media.delete=1 where id = :id;
            DELETE mediaItemMap from mediaItemMap where mediaId = :id;";
}
if($type == "key"){
    $sql = "DELETE FROM dictionary where id = :id";

    //echo("deleting key:");
    //echo($sql);

    //echo (" delete string = ". $delete_ids_string);
}

$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $delete_ids_string, PDO::PARAM_STR);
$st->execute();

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'delete item good');
}
else{
    echo returnStatus(0 , 'delete item fail');
}

?>
