<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select roomIpMap.roomName, bsp_update_log.`event`, bsp_update_log.lastUpdate, bsp_update_log.`updateCheck`, bsp_update_log.`updateGetLangForKey`, bsp_update_log.`updateGetItemList`, bsp_update_log.`updateGetPhoto` from bsp_update_log RIGHT JOIN roomIpMap on bsp_update_log.sourceIP = roomIpMap.ip ORDER BY roomIpMap.roomName ASC;";
$st = $conn->prepare ( $sql );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'Get BSP Status ok!',$list);
else
    echo returnStatus(0 , 'Get BSP Status fail!');

$conn = null;

?>
