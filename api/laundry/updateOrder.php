<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
//include("checkSession.php");

$id = getValueFromArray($_REQUEST, 'orderId', '');
$status = getValueFromArray($_REQUEST, 'status', 0);

$email = getValueFromArray($_SESSION, 'email', '');

function getValueFromArray($arr, $key, $default = '')
{
	if ( !empty($arr) ) {
		return isset( $arr[$key] ) ? $arr[$key] : $default;
	}
	return $default;
}

if ( empty($id) ) {
    echo returnStatus(0, 'missing order Id');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $sql = "UPDATE laundry_orders SET status=:status, lastUpdateBy=:email, lastUpdate=now()";
    $sqlEnd = "where id = :orderId;";

    $sql = $sql.$sqlEnd;

    $st = $conn->prepare ( $sql );
    $st->bindValue( ":orderId", $id, PDO::PARAM_STR );
    $st->bindValue( ":status", $status, PDO::PARAM_INT );
    $st->bindValue( ":email", $email, PDO::PARAM_STR );

    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    if ( $st->fetchColumn() > 0 || $st->rowCount() > 0 ){
        echo returnStatus(1, 'update Order OK', $list);
    }
    else {
        echo returnStatus(0, 'update Order fail', $list);
    }
}
return 0;

?>
