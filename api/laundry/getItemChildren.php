
<?php

require( "../../config.php" );

ini_set( "display_errors", true );

require( "../../php/inc.appvars.php" );
require( "../../php/func_nx.php" );

header('Content-Type: text/html; charset=utf-8');

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

//echoLine("Connected successfully");
		
//$optionSets = getRecord("47bd8912-9126-11e6-ad79-e4115bbe5c3e");
//echoLine("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
//echoLine($optionSets);
//echoLine("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
//$record = getRecord("840e32f0-0e4b-11e5-9df3-984be11049af");

$itemId = isset($_REQUEST["itemId"])?$_REQUEST["itemId"]:null;
$optionSetId = isset($_REQUEST["optionSetId"])?$_REQUEST["optionSetId"]:null;

if($itemId != null && $optionSetId != null){
	$optionSets = getRecord($optionSetId);
	$record = getRecord($itemId);
	echo $record;
}

//echoLine("Connection ended");

$conn = null;


function echoLine ($text) {
	echo $text;
	echo "<br>";
}

function getRecord($id) {
	$record = json_decode(getItem($id), true);
	if (!empty($record)) {
		$firstRecord = json_decode($record[0], true);
		return json_encode( getRecordDetail($firstRecord, 0) );
	}
	return json_encode( "" );
}

function getRecordDetail($record, $layer) {
	$recordDict = json_decode(getDict($record["titleId"]), true);
	if (!empty($recordDict)) {
		$firstDict = json_decode($recordDict[0], true);
		$record["dictionary"] = $firstDict;
//		echoLine(str_repeat(".....", $layer) . "Record - " . $firstDict["en"]);
		if (!empty($record["optionSetIds"]) && !empty($GLOBALS["optionSets"])) {
			$splits = explode(",", $record["optionSetIds"]);
			$sets = array();
			foreach ($splits as $id) {
				$set = getOptionSet($id);
//				echoLine("Option Set : " . $set["dictionary"]["en"]);
				$sets[] = $set;

			}
			$record["optionSets"] = $sets;
		}
	}
        $recordMediaMap = json_decode(getMediaMap($record["id"]), true);

	$medias = array();
	foreach ($recordMediaMap as $v) {
		$mediaMap = json_decode($v, true);
		$mediaId = $mediaMap["mediaId"];
		$media = json_decode(getMedia($mediaId), true);
		if (!empty($media)) {
			$firstMedia = json_decode($media[0], true);
//			echoLine("Image : " . $firstMedia["fileName"]);
			$medias[] = $firstMedia;
		}
	}

	$record["media"] = $medias;
	$record["child"] = getChildRecords($record["id"], $layer);

	return $record;
}

function getChildRecords ($id, $layer) {
        $childRecords = json_decode(getChildItems($id), true);

	$childs = array();
	if (!empty($childRecords)) {
		foreach ($childRecords as $r) {
			$child = json_decode($r, true);
			$childs[] = getRecordDetail($child, $layer + 1);
		}
	}
	return $childs;
}

function getOptionSet ($id) {
	$arr = json_decode($GLOBALS["optionSets"], true);
	$results = array_filter($arr['child'], function($child) use ($id) {
		return $child['id'] == $id;
	});
	if (!empty($results)) {
		return reset($results);
	}
	return null;
}

function getDict ($id) {
        return sqlGet("SELECT * FROM dictionary where dictionary.id like :id", $id);
}

function getItem ($id) {
        return sqlGet("SELECT * FROM items where items.id like :id", $id);
}

function getChildItems ($id) {
        return sqlGet("SELECT * FROM items where items.parentId like :id and items.enable = 1 order by `order`", $id);
}

function getMediaMap ($id) {
        return sqlGet("SELECT * FROM mediaItemMap where mediaItemMap.itemId like :id", $id);
}

function getMedia ($id) {
        return sqlGet("SELECT * FROM media where media.id like :id", $id);
}


function sqlGet ($sql, $id) {
        global $conn;
		
        $result = $conn->prepare($sql);
		$result->execute(array('id' => $id));
        $data = array();
        if (!$result) {
                trigger_error('Invalid query: ' . $conn->error);
        } else if ($result->rowCount() > 0) {
                // output data of each row
                while ( $row = $result->fetch(PDO::FETCH_ASSOC) ){
                        $data[] = json_encode($row);
                }
                //echoLine("$result->rowCount() results");
        } else {
                //echoLine("0 results");
        }

        return json_encode( $data );
}

exit;
