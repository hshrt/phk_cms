<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$room = getValueFromArray($_REQUEST, 'room', '');
$laundryIdList = getValueFromArray($_REQUEST, 'laundryIdList', '');
$pickupTime = getValueFromArray($_REQUEST, 'pickupTime', '');
$orderTime = getValueFromArray($_REQUEST, 'orderTime', 'now');
$requirement = getValueFromArray($_REQUEST, 'requirement', '');
$status = getValueFromArray($_REQUEST, 'status', 0);
$lastUpdateBy = getValueFromArray($_REQUEST, 'lastUpdateBy', 'BSP');

/*************************************
** new check on the order time diff - by william at 2018-04-23
*************************************/

function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    $h_diff = $interval->format("%h") * 60;
    $m_diff = $interval->format("%i");

    return intval($h_diff) + intval($m_diff);
}

function getValueFromArray($arr, $key, $default = '')
{
	if ( !empty($arr) ) {
		return isset( $arr[$key] ) ? $arr[$key] : $default;
	}
	return $default;
}

$ordPhasedTime=0;
$ordPhasedTime=date("Y-m-d H:i", strtotime(urldecode($orderTime)));

if ( $orderTime == 'now' ) {
    $orderTime = $ordPhasedTime;
}

if ( empty($room) ) {
    echo returnStatus(0, 'missing room number');
}
else if( intval(dateDifference($ordPhasedTime, date("Y-m-d H:i"), "%h %i")) > 10 ) {
    echo returnStatus(0, 'The order time is wrong.');
}
else if( strlen($laundryIdList) == 0 ) {
    echo returnStatus(0, 'The laundry Id List is empty.');
}
else {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    // *****create Order
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];
    $hotsos = "166";
    $itemCategory = array();

    $items = json_decode(urldecode($laundryIdList), true);
    foreach ($items as $item) {
        $itemId = $item["id"];
        $itemOptionIds = "";
        if (array_key_exists("optionIds", $item)) {
            $itemOptionIds = $item["optionIds"];
        }
        $itemPlastic = $item["plasticCover"];
        $itemExpress = $item["expressOrder"];
        if ($itemExpress == "1") {
            $hotsos = "165";
        }
        $itemQuantity = $item["quantity"];

        $checkCategorySql = "select en from dictionary as dict";
        $checkCategorySql .= " left join items as i on i.titleId like dict.id";
        $checkCategorySql .= " left join items as son on son.parentId like i.id";
        $checkCategorySql .= " left join items as grandson on grandson.parentId like son.id";
        $checkCategorySql .= " where grandson.id like '" . $itemId . "'";

        $checkCategorySt = $conn->prepare( $checkCategorySql );
        $checkCategorySt->execute();
        $checkCategoryValue = $checkCategorySt->fetch();
            $category = $checkCategoryValue["en"];

            $categoryCount = 0;
            if (array_key_exists($category, $itemCategory)) {
                $categoryCount = $itemCategory[$category];
            }
            $categoryCount += $itemQuantity;

            $itemCategory[$category] = $categoryCount;

        $sql = "insert laundry_orderitems (orderId, itemId, itemOptionIds, quantity, serviceType, plasticCover, status, lastUpdate, lastUpdateBy)";
        $sql .= " VALUES";
        $sql .= " (:orderId, :itemId, :itemOptionIds, :quantity, :serviceType, :plasticCover, :status, now(), :lastUpdateBy)";

        $st = $conn->prepare ( $sql );
        $st->bindValue( ":orderId", $uuid, PDO::PARAM_STR );
        $st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
        $st->bindValue( ":itemOptionIds", $itemOptionIds, PDO::PARAM_STR );
        $st->bindValue( ":quantity", (int)$itemQuantity, PDO::PARAM_INT );
        $st->bindValue( ":serviceType", (int)$itemExpress, PDO::PARAM_INT );
        $st->bindValue( ":plasticCover", (int)$itemPlastic, PDO::PARAM_INT );
        $st->bindValue( ":status", $status, PDO::PARAM_STR );
        $st->bindValue( ":lastUpdateBy", $lastUpdateBy, PDO::PARAM_STR );

        $st->execute();

        if ( $st->fetchColumn() > 0 || $st->rowCount() > 0 ){

        } else {
            echo returnStatus(0, 'Order items submission Failed');
            return 1;
        }
    }

    $sql = "insert laundry_orders (id, room, pickupTime, orderTime, requirement, status, lastUpdate, lastUpdateBy)";
    $sql .= " VALUES";
    $sql .= " (:id,:room,:pickupTime,:orderTime,:requirement,:status,now(),:lastUpdateBy)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":room", $room, PDO::PARAM_STR );
    $st->bindValue( ":pickupTime", $pickupTime, PDO::PARAM_STR );
    $st->bindValue( ":orderTime", $orderTime, PDO::PARAM_STR );
    $st->bindValue( ":requirement", $requirement, PDO::PARAM_STR );
    $st->bindValue( ":status", $status, PDO::PARAM_INT );
    $st->bindValue( ":lastUpdateBy", $lastUpdateBy, PDO::PARAM_STR );
    $st->execute();

    if ( $st->fetchColumn() > 0 || $st->rowCount() > 0 ){

        echo returnStatus(1, 'Order submission OK');

		/*
        // hard coded the path
		$url = "http://127.0.0.1/cmspbj/api/laundry/getOrder.php?status=0&room=".$room."&date=".date("Y-m-d");

        $pullorder = file_get_contents($url);
        //var_dump(json_decode($justOrder, true));

        $json=json_decode($pullorder, true);

		$text = "";
        $file =  "/tmp/".$room."laundrymail.txt";

        $myfile = fopen($file, "w") or die("Unable to open file!");
        fwrite($myfile, $text);
        fclose($myfile);

        foreach($json['data'] as $key=>$item) {

                if ( array_key_exists("choices",$item)) {

                        $text = $item['item']." - ".$item['choices']." [ deliveryTime:".$item['deliveryTime']." ]\r\n";
                } else {
                        $text = $item['item']." [ deliveryTime:".$item['deliveryTime']." ]\r\n";
                }

                echo $text;

                $myfile = fopen($file, "a") or die("Unable to open file!");
                fwrite($myfile, $text);
                fclose($myfile);

        }


        exec(" cat /tmp/".$room."irdmail.txt | mail -s 'IRD mail - ".$room."' 'williamchan@peninsula.com,roomservicepbj@peninsula.com,jennywang@peninsula.com,pbjhsia@peninsula.com' ");
		*/
    }
    else {
        echo returnStatus(0, 'Order submission fail');
    }

    $extra = "";
    foreach ($itemCategory as $k => $v) {
        if (strlen($extra) == 0) {
            $extra .= " - ";
        } else {
            $extra .= ", ";
        }
        $extra .= $k . " (" . strval($v) . ")";
    }
    $hotsos_url = "http://172.30.6.13/hotsos.php?id=tempuser&pw=temppw&room=" .
                    $room . "&value=" . $hotsos;

//    print_r($hotsos_url);

    file_get_contents($hotsos_url);
}
return 0;

?>
