<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
$_SESSION['when'] = time();

$date = getValueFromArray($_REQUEST, 'date', '');
$status = getValueFromArray($_REQUEST, 'status', '');
$room = getValueFromArray($_REQUEST, 'room', '');
$device = getValueFromArray($_REQUEST, 'device', '');

function getValueFromArray($arr, $key, $default = '')
{
	if ( !empty($arr) ) {
		return isset( $arr[$key] ) ? $arr[$key] : $default;
	}
	return $default;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$base = "SELECT id, room, refId, laundryIdList, pickupTime, orderTime, requirement, status, lastUpdate, lastUpdateBy from laundry_orders";
$base .= " left join ( SELECT orderId, JSON_ARRAYAGG(JSON_OBJECT(";
$base .= "    'itemId', itemId, 'itemOptionIds', itemOptionIds, 'quantity', quantity, 'serviceType', serviceType, 'plasticCover', plasticCover,";
$base .= "    'status', status, 'deleted', deleted, 'lastUpdate', p1.lastUpdate, 'lastUpdateBy', p1.lastUpdateBy, 'item', d.en, 'choice', choice,";
$base .= "    'parent', pd.en, 'grandparent', gpd.en";
$base .= " )) as laundryIdList FROM laundry_orderitems as p1";

$base .= "   left join ( SELECT optionTable.id, GROUP_CONCAT(d2.en separator ', ') as choice FROM ";
$base .= "     ( SELECT laundry_orderitems.id, trim(SUBSTRING_INDEX(SUBSTRING_INDEX(itemOptionIds, ',', n.digit), ',', -1)) val FROM laundry_orderitems";
$base .= "       left join BSPPHK.laundry_orders as suborder on suborder.id = laundry_orderitems.orderId";
$base .= "       JOIN (SELECT 1 digit UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) n ON CHAR_LENGTH(itemOptionIds) - CHAR_LENGTH(REPLACE(itemOptionIds, ',' , '')) >= n.digit - 1";
if ( $device == "BSP" ) {
        $base .= " where suborder.room = :room && suborder.status!=2 && suborder.status!=9 && suborder.deleted!=1";
} else {
        if ( strlen($date) > 0 ) {
                $base .= " where suborder.orderTime >= :date1 && suborder.orderTime < DATE(DATE_ADD(:date1, interval 1 day)) && suborder.status != 9";
        }
        if ( strlen($status) > 0 ) {
                $base .= " && suborder.status = :status";
        }
        if ( strlen($room) > 0 ) {
                $base .= " && suborder.room = :room";
        }
}
$base .= "     ) as optionTable";
$base .= "     left join items as i2 on i2.id = optionTable.val";
$base .= "     left join dictionary as d2 on i2.titleId = d2.id";
$base .= "     group by optionTable.id";
$base .= "   ) as p2 on p2.id = p1.id";

$base .= "   left join items as i on i.id = p1.itemId";
$base .= "   left join dictionary as d on i.titleId = d.id";
$base .= "   left join items as pi on pi.id = i.parentId";
$base .= "   left join dictionary as pd on pi.titleId = pd.id";
$base .= "   left join items as gpi on gpi.id = pi.parentId";
$base .= "   left join dictionary as gpd on gpi.titleId = gpd.id";
$base .= "   group by orderId";
$base .= "   order by gpd.id";
$base .= " ) AS orderitems ON laundry_orders.id = orderitems.orderId";

$sql = $base;

if ( strlen($date) > 0 ) {
	$sql = $sql." where orderTime >= :date1 && orderTime < DATE(DATE_ADD(:date1, interval 1 day)) && status != 9";
}
if ( strlen($status) > 0 ) {
	$sql = $sql." && status = :status";
}
if ( strlen($room) > 0 ) {
	$sql = $sql." && room = :room";
}
// only for BSP
if ( $device == "BSP" ) {
	$sql = $base." where room = :room && status!=2 && status!=9 && deleted!=1";
}

$orderBy = " order by orderTime DESC";

$sql = $sql.$orderBy;

$st = $conn->prepare ( $sql );

if ( strlen($date) > 0 ) {
	$st->bindValue(":date1", $date, PDO::PARAM_STR);
}
if ( strlen($status) > 0 ) {
	$st->bindValue(":status", $status, PDO::PARAM_INT);
}
if ( strlen($room) > 0 ) {
	$st->bindValue(":room", $room, PDO::PARAM_STR); 
}

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
	$list[] = $row;
}

if ( $st->fetchColumn() > 0 || $st->rowCount() > 0 ) {
	echo returnStatus(1, 'get Order OK', $list);
}
else {
	echo returnStatus(0, 'get Order fail', $list);
}

return 0;

?>
