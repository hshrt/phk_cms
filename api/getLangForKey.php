<?php 

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");

if(isset($_REQUEST['id'])){
    $id= $_REQUEST["id"];
}

$appVersion = isset($_REQUEST["appVersion"])?$_REQUEST["appVersion"]:null;
if($appVersion!=null){
    $appVerNumArr=explode('.', $appVersion);
}

$getAllKey = $getCount = $getAll = null;

if(isset($_REQUEST['getAllKey'])){
    $getAllKey = $_REQUEST['getAllKey'];
}

if(isset($_REQUEST['getCount'])){
    $getCount = $_REQUEST['getCount'];
}

if(isset($_REQUEST['getAll'])){
    $getAll = $_REQUEST['getAll'];
}

if (!empty($getAll)) {
    $mem_var = new Memcache;
    $mem_var->connect("localhost", 11211);

    $result = $mem_var->get(ALL_DICT);

    if ($result != null) {
        echo returnStatus(1, 'good', $result);
        return;
    }
}

$page = 0;
$itemPerPage = 15;

if(isset($_REQUEST['page'])){
    $page = $_REQUEST['page'];
}
if(isset($_REQUEST['itemPerPage'])){
    $itemPerPage = $_REQUEST['itemPerPage'];
}

$forMsg = isset($_REQUEST["forMsg"])?$_REQUEST["forMsg"]:0;

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if(!empty($getCount)){
    $sql = "select  count(*) as totalKeysNum from dictionary where dictionary.key!='' ";
}
else if(!empty($getAll)){
    $sql = "SELECT  id,dictionary.key,en as E , zh_hk as CT, zh_cn as CS, jp as J, fr as F, ar as A, es as S, de as G, ko as K, 
ru as R, pt as P , tr as I from dictionary";
}
else if(empty($getAllKey)){
    $sql = "SELECT en, zh_hk, zh_cn, jp, fr, ar, es, de, ko, ru, pt, tr from dictionary where id = :id";

    if($forMsg == 1){
        $sql = "SELECT en, zh_hk, zh_cn, jp, fr, ar, es, de, ko, ru, pt, tr from dictionary_msg where id = :id";
    }
}
else{
    $sql = "SELECT id,dictionary.key,en, zh_hk, zh_cn, jp, fr, ar, es, de, ko, ru, pt, tr from dictionary where dictionary.key!='' 
order by dictionary.en ASC";
}

$st = $conn->prepare ($sql);

if(!empty($getCount)){

}
else if(!empty($getAll)){

}
else if(empty($getAllKey)){
    $st->bindValue( ":id", $id, PDO::PARAM_STR);
}
else{
    //$st->bindValue( ":page", $page*1*$itemPerPage, PDO::PARAM_INT);
    //$st->bindValue( ":itemPerPage", $itemPerPage*1, PDO::PARAM_INT);
}

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

$conn = null;


echo returnStatus(1, 'good', $list);

?>
