<!DOCTYPE html>
<html lang="en">
<head>
    <title>Print article</title>

    <link rel="shortcut icon" href="../../images/favicon.ico">
    <link rel="apple-touch-icon" href="../../images/favicon.ico">

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="stylesheet" type="text/css" href="../../css/main.css" />

    <link rel="stylesheet" type="text/css" href="../../css/simplePagination.css" />

    <link rel="stylesheet" type="text/css" href="../../3rdparty/timepicker/jquery.timepicker.min.css" />

    <!-- Theme style -->
    <link href="../../css/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the
    load. -->
    <link href="../../css/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!--<link href="css/extra.css" rel="stylesheet" type="text/css" />-->

    <!--image crop Library-->
    <link href="../../3rdparty/cropper/cropper.css" rel="stylesheet" type="text/css" />


    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <!-- Bootstrap 3.3.2 -->
    <link href="../../3rdparty/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font- awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />



    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
    <script src="../../3rdparty/jquery-2.1.1.min.js"></script>
</head>
<?php $itemId= $_REQUEST["itemId"];?>
<?php $lang= $_REQUEST["lang"];
//following is for BSP version prior to 5.0.0
    if ($lang == "ja") {$lang = "jp";}
    if ($lang == "zh-cn") {$lang = "zh_cn";}
    if ($lang == "zh-tw") {$lang = "zh_hk";}
?>
<div class="a4Container">
    <div id="header">
        <img id="pen_logo" src="../../images/peninsula.jpg" ?="">
    </div>
    <div id="content">

        <div class="container-fluid">

                <div class="thirdCol" id="itemTitle"></div>

        </div>
        <div class="container-fluid">
            <img id="itemImage">
        </div>
        <div class="container-fluid">
            <div id="des">
            </div>
        </div>
    </div>
    <div class="footer">The Peninsula Hong Kong,
        Salisbury Road, Kowloon, Hong Kong, SAR<br>
        Telephone: +852 2920 2888
        E-mail: phk@peninsula.com

    </div>
    <!--<div class="footer">The Peninsula Chicago,
        108 East Superior Street (at North Michigan Avenue), Chicago, Illinois 60611, USA<br>
        Toll-free: +1 312 337 2888
        E-mail: pch@peninsula.com

    </div>-->
</div>

<script>
    var App = {};
    
    $.ajax({
        url : "../getItemList.php",
        method : "POST",
        dataType: "json",
        data : {itemId:"<?php echo($itemId);?>",
            lang:"<?php echo($lang);?>"}
    }).success(function(json){
        console.log("firstItemList = " + json);
        App.currentItem = json.data[0];
        console.log("App.currentItem.description_en = " + App.currentItem.description_en);
        $("#itemTitle").text( App.currentItem.title_en);

        $("#itemImage").attr("src", "../../upload/"+App.currentItem.fileName+".jpg");
        App.currentItem.description_en = App.currentItem.description_en.replace(/\n/g, "<br />");
        $("#des").html(App.currentItem.description_en);

    }).error(function(d){
        console.log('error');
        console.log(d);
    });

</script>