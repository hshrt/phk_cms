<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$room = isset($_REQUEST['room'])?$_REQUEST['room']:null;
$isReLogin = isset($_REQUEST['isReLogin'])?$_REQUEST['isReLogin']:null;

if ( empty($room)){
    echo returnStatus(0, 'missing room number');
    exit;
}

if ( empty($isReLogin)) {
    echo returnStatus(0, 'missing isReLogin status');
    exit;
}


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE allroom SET isReLogin = :isReLogin where room = :room";
$st = $conn->prepare ( $sql );

$st->bindValue( ":room", $room, PDO::PARAM_STR );
$st->bindValue( ":isReLogin", $isReLogin, PDO::PARAM_STR );


$st->execute();

echo returnStatus(1, 'update ReLogin ok');

$conn = null;

?>