<?php
require( "../config.php" );

ini_set( "display_errors", true );

require( "../php/inc.appvars.php" );
require( "../php/func_nx.php" );

$appVersion = isset($_REQUEST["appVersion"])?$_REQUEST["appVersion"]:null;
if ($appVersion != null) {
	$appVerNumArr = explode('.', $appVersion);
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$mem_var = new Memcache;
$mem_var->connect("localhost", 11211);

$sql = "select items.*,t2.image,d4.iconName,d4.ext,d5.videoThumbnail, element.price AS infrasys_price, element.type AS infrasys_type from items

    LEFT JOIN (select id,itemId, filename as image, prefer from media, mediaItemMap
            where id = mediaId and media.delete != 1 and mediaItemMap.prefer = 1 order by lastUpdateTime DESC) t2
            on items.id = t2.itemId

    LEFT JOIN (select fileName AS iconName, fileExt as ext, m2.itemId from media inner join mediaItemMap m2
            on media.id = m2.mediaId && m2.isIcon = 1) d4
            ON items.id = d4.itemId

    LEFT JOIN (select fileName AS videoThumbnail, fileExt as ext, m3.itemId from media inner join mediaItemMap m3
            on media.id = m3.mediaId && m3.isThumbnail = 1) d5
            ON items.id = d5.itemId

    LEFT JOIN itemInfrasysMap map1
    ON items.id = map1.item_id

    LEFT JOIN infrasys_items element
    ON map1.infrasys_id = element.code AND map1.infrasys_type = element.type

    GROUP BY items.id

    order by items.order ASC, items.lastUpdate ASC";

$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
	$list[] = $row;
}

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
	$mem_var->set(ALL_ON_OFF_ITEM, $list);
	echo "done";
}

?>
