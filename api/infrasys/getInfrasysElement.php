<?php
require("../../config.php");
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
$_SESSION['when'] = time();



    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");
    // \"PHKM01560\

    $sql = "SELECT code, type, name, price from infrasys_items where code != :code1 and code != :code2 and code != :code3 order by name ASC";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":code1", "PHKM01560", PDO::PARAM_STR );
    $st->bindValue( ":code2", "PHKM01814", PDO::PARAM_STR );
    $st->bindValue( ":code3", "PHKM01815", PDO::PARAM_STR );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    echo returnStatus(1, 'get data OK',$list);

?>
