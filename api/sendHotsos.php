<?php

ini_set( "display_errors", true );
require("../config.php");

require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();

$myid="tempuser";
$mypw="temppw";
$hotsosIP="172.30.6.13";
$commandList = array (
	"EXTRAWATER" => "111",
	"EXTRAICE" => "112",
	"IRON" => "113",
	"MULTIADAPTER" => "114",
	"3DGLASSES" => "115",
	"ROOMSERVICE" => "188",
);

$room = getValueFromArray( $_REQUEST, 'room', '' );
$command = getValueFromArray ( $_REQUEST, 'command', '' );
$value = getValueFromArray( $commandList, $command, $command );
$value = trimHotsosHead( $value );

function getValueFromArray($arr, $key, $default = '')
{
	if ( !empty($arr) ) {
		if ( isset( $arr[$key] ) ) {
			return $arr[$key];
		}
	}
	return $default;
}

function trimHotsosHead($value)
{
	$compare = "HOTSOS ";
	if (substr( $value, 0, strlen($compare)) === $compare) {
		return substr( $value, strlen($compare) );
	}
	return $value;
}

// http://[hotsosIP]/hotsos.php?room=[room]&value=[value]&id=[id]&pw=[pw] 
$hotsos_url = sprintf("http://%s/hotsos.php?room=%s&value=%s&id=%s&pw=%s", $hotsosIP, $room, $value, $myid, $mypw);
file_get_contents($hotsos_url);

return 0;

?>
